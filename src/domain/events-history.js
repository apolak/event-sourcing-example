//@flow

class EventsHistory<T> {
  aggregateId: T;
	events: Array<Object>;

	constructor(aggregateId: T, events: Array<Object>) {
		this.aggregateId = aggregateId;
		this.events = events;
	}
}

module.exports = EventsHistory;
