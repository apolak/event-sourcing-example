//@flow

type StatusMap = {
  POSITIVE: string,
  NEGATIVE: string,
  NOT_PERFORMED: string
};

class AnalysisStatus {
  static availableTypes: StatusMap;

  type: string;

  constructor(type: string) {
    this.type = type;
  }

  static negative(): AnalysisStatus {
    return new AnalysisStatus(AnalysisStatus.availableTypes.POSITIVE);
  }

  static positive(): AnalysisStatus {
    return new AnalysisStatus(AnalysisStatus.availableTypes.POSITIVE);
  }

  static notPerformed(): AnalysisStatus {
    return new AnalysisStatus(AnalysisStatus.availableTypes.NOT_PERFORMED);
  }

  static fromType(type: string): AnalysisStatus {
    if (AnalysisStatus.availableTypes[type] === undefined) {
      throw new Error(`Status "${type}" is not supported.`);
    }

    return new AnalysisStatus(type);
  }

  is(type: string): boolean {
    return this.type === type;
  }
}

AnalysisStatus.availableTypes = {
  POSITIVE: 'POSITIVE',
  NEGATIVE: 'NEGATIVE',
  NOT_PERFORMED: 'NOT_PERFORMED'
};

module.exports = AnalysisStatus;
