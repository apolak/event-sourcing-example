//@flow

import uuidV4 from 'uuid/v4';
import type { Id } from '../id';

class AnalysisId implements Id<AnalysisId> {
  id: string;

  constructor(id: string) {
    if (!AnalysisId.isValidId(id)) {
      throw new Error(`ID must be valid uuidV4, "${id}" given`);
    }

    this.id = id;
  }

  static generate(): AnalysisId {
    return new AnalysisId(uuidV4());
  }

  static fromString(id: string): AnalysisId {
    return new AnalysisId(id);
  }

  static isValidId(id: string): boolean {
    return /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(id);
  }
}

module.exports = AnalysisId;
