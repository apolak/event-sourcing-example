//@flow

type TypesMap = {
  'personal-background': string,
  'previous-credits': string,
  earnings: string
};

class AnalysisType {
  static availableTypes: TypesMap;

  type: string;

  constructor(type: string) {
    this.type = type;
  }

  static personalBackground(): AnalysisType {
    return new AnalysisType(AnalysisType.availableTypes['personal-background']);
  }

  static previousCredits(): AnalysisType {
    return new AnalysisType(AnalysisType.availableTypes['previous-credits']);
  }

  static earnings(): AnalysisType {
    return new AnalysisType(AnalysisType.availableTypes.earnings);
  }

  static fromType(type: string): AnalysisType {
    if (AnalysisType.availableTypes[type] === undefined) {
      throw new Error(`Type "${type}" is not supported.`);
    }

    return new AnalysisType(type);
  }

  is(type: string): boolean {
    return this.type === type;
  }
}

AnalysisType.availableTypes = {
  'personal-background': 'personal-background',
  'previous-credits': 'previous-credits',
  earnings: 'earnings'
};

module.exports = AnalysisType;
