//@flow
import CreditApplicationId from './credit-application/credit-application-id';
import UserId from './user-id';
import EventsHistory from './events-history';
import Event from './event';
import Status from './credit-application/status';
import PersonalData from './credit-application/personal-data';
import FinancialData from './credit-application/financial-data';
import CreditInfo from './credit-application/credit-info';
import CreditApplicationProcedureResult from './credit-application/credit-application-procedure-result';

import eventHandlers from './handlers/event-handlers/credit-application';
import type { EventHandler } from './handlers/event-handler';

import Events from './events/credit-application';

class CreditApplication {
	creditApplicationId: CreditApplicationId;
	recentEvents: Array<Event>;
	creditApplicationStatus: Status;
	personalData: PersonalData;
	financialData: FinancialData;
	creditInfo: CreditInfo;
	userId: ?UserId;

	constructor(creditApplicationId: CreditApplicationId) {
		this.creditApplicationId = creditApplicationId;
		this.recentEvents = [];
		this.creditApplicationStatus = Status.created();
		this.personalData = PersonalData.empty();
		this.financialData = FinancialData.empty();
		this.creditInfo = CreditInfo.empty();
		this.userId = null;
	}

	static reconstituteFrom(eventsHistory: EventsHistory<CreditApplicationId>) {
		let order = new CreditApplication(eventsHistory.aggregateId);

		eventsHistory.events.forEach((event: Event) => order.applyEvent(event));

		return order;
	}

	openProcedure(userId: UserId) {
		const openProcedureEvent: Event = Events.v2.OpenCreditApplicationProcedureEvent.Instance.withoutCreationDate(
		  this.creditApplicationId.id,
      userId.id
    );

		this.recentEvents.push(openProcedureEvent);
		this.applyEvent(openProcedureEvent);
	}

  fillPersonalData(personalDataDto: Object) {
	  const personalData: PersonalData = PersonalData.fromData(personalDataDto);

	  if(this.creditApplicationStatus.is(Status.availableTypes.PROCEDURE_OPENED)) {
      const personalDataFilledEvent: Event = Events.v1.PersonalDataFilledEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        personalData
      );

      this.recentEvents.push(personalDataFilledEvent);
      this.applyEvent(personalDataFilledEvent);
    }
  }

  fillFinancialData(financialDataDto: Object) {
    const financialData: FinancialData = FinancialData.fromData(financialDataDto);

	  if (this.personalData.isFilled()) {
      const financialDataFilledEvent: Event = Events.v1.FinancialDataFilledEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        financialData
      );

      this.recentEvents.push(financialDataFilledEvent);
      this.applyEvent(financialDataFilledEvent);
    }
  }

  fillCreditInfo(creditInfoDto: Object) {
    const creditInfo: CreditInfo = CreditInfo.fromData(creditInfoDto);

	  if (this.financialData.isFilled()) {
      const creditInfoFilledEvent: Event = Events.v1.CreditInfoFilledEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        creditInfo
      );

      this.recentEvents.push(creditInfoFilledEvent);
      this.applyEvent(creditInfoFilledEvent);
    }
  }

  checkConsumerCreditHistory() {
	  if (this.creditInfo.isFilled()) {
      const creditHistoryCheckEvent: Event = Events.v1.CreditHistoryCheckEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        this.personalData.pesel
      );

      this.recentEvents.push(creditHistoryCheckEvent);
      this.applyEvent(creditHistoryCheckEvent);
    }
  }

	acceptToAnalysis() {
	  if (this.creditApplicationStatus.is(Status.availableTypes.CREDIT_HISTORY_CHECK_STARTED)) {
      const creditHistoryCheckAcceptedEvent: Event = Events.v1.CreditHistoryCheckAcceptedEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id
      );

      this.recentEvents.push(creditHistoryCheckAcceptedEvent);
      this.applyEvent(creditHistoryCheckAcceptedEvent);
    }
	}

  rejectBecauseOfHistoryCheck(reason: string) {
    if (this.creditApplicationStatus.is(Status.availableTypes.CREDIT_HISTORY_CHECK_STARTED)) {
      const creditHistoryCheckRejectedEvent: Event = Events.v1.CreditHistoryCheckRejectedEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        reason
      );

      const rejectApplicationEvent: Event = Events.v1.CreditApplicationRejectedEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        reason
      );

      this.recentEvents.push(creditHistoryCheckRejectedEvent);
      this.recentEvents.push(rejectApplicationEvent);
      this.applyEvent(creditHistoryCheckRejectedEvent);
      this.applyEvent(rejectApplicationEvent);
    }
  }

	forwardToAnalysis() {
    if (this.creditApplicationStatus.is(Status.availableTypes.CREDIT_HISTORY_CHECK_POSITIVE)) {
      const forwardedToAnalysisEvent: Event = Events.v1.ForwardedToAnalysisEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id
      );

      this.recentEvents.push(forwardedToAnalysisEvent);
      this.applyEvent(forwardedToAnalysisEvent);
    }
	}

	connectWithAnalysis(analysisId: string) {
		if (analysisId) {
      const analysisStartedEvent: Event = Events.v1.AnalysisStartedEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        analysisId
      );

      this.recentEvents.push(analysisStartedEvent);
      this.applyEvent(analysisStartedEvent);
    }
	}

  readyForContract() {
    const readyForContractEvent: Event = Events.v1.ReadyForContractEvent.Instance.withoutCreationDate(
      this.creditApplicationId.id
    );

    this.recentEvents.push(readyForContractEvent);
    this.applyEvent(readyForContractEvent);
  }

  rejectBecauseOfAnalysis(reason: string) {
    const analysisRejectedEvent: Event = Events.v1.AnalysisRejectedEvent.Instance.withoutCreationDate(
      this.creditApplicationId.id,
      reason
    );

    const rejectApplicationEvent: Event = Events.v1.CreditApplicationRejectedEvent.Instance.withoutCreationDate(
      this.creditApplicationId.id,
      reason
    );

    this.recentEvents.push(analysisRejectedEvent);
    this.recentEvents.push(rejectApplicationEvent);
    this.applyEvent(analysisRejectedEvent);
    this.applyEvent(rejectApplicationEvent);
  }

  attachContract(contractId: string) {
    const readyForPresentationEvent: Event = Events.v1.ReadyForPresentationEvent.Instance.withoutCreationDate(
      this.creditApplicationId.id,
	    contractId
    );

    this.recentEvents.push(readyForPresentationEvent);
    this.applyEvent(readyForPresentationEvent);
  }

  markAsPresentedToCustomer() {
    const contractPresentedEvent: Event = Events.v1.ContractPresentedEvent.Instance.withoutCreationDate(
      this.creditApplicationId.id
    );

    this.recentEvents.push(contractPresentedEvent);
    this.applyEvent(contractPresentedEvent);
  }

  acceptContract() {
    const contractAcceptedEvent: Event = Events.v1.ContractAcceptedEvent.Instance.withoutCreationDate(
      this.creditApplicationId.id
    );

    const closeProcedureEvent: Event = Events.v1.CloseCreditApplicationProcedureEvent.Instance.withoutCreationDate(
      this.creditApplicationId.id,
      CreditApplicationProcedureResult.success
    );

    this.recentEvents.push(contractAcceptedEvent);
    this.recentEvents.push(closeProcedureEvent);
    this.applyEvent(contractAcceptedEvent);
    this.applyEvent(closeProcedureEvent);
  }

  rejectContract(reason: string) {
    const contractRejectedEvent: Event = Events.v1.ContractRejectedEvent.Instance.withoutCreationDate(
      this.creditApplicationId.id,
	    reason
    );

    const closeProcedureEvent: Event = Events.v1.CloseCreditApplicationProcedureEvent.Instance.withoutCreationDate(
      this.creditApplicationId.id,
      CreditApplicationProcedureResult.failure
    );

    this.recentEvents.push(contractRejectedEvent);
    this.recentEvents.push(closeProcedureEvent);
    this.applyEvent(contractRejectedEvent);
    this.applyEvent(closeProcedureEvent);
  }

  closeProcedure() {
    const closeProcedureEvent: Event = Events.v1.CloseCreditApplicationProcedureEvent.Instance.withoutCreationDate(
      this.creditApplicationId.id,
      CreditApplicationProcedureResult.failure
    );

    this.recentEvents.push(closeProcedureEvent);
    this.applyEvent(closeProcedureEvent);
  }

	applyEvent(event: Event) {
		const handler = eventHandlers.find((handler: EventHandler<CreditApplication>) => handler.canHandle(event));

		if (handler === undefined) {
			throw new Error(`Could not find handler for event ${event.name}`);
		}

		handler.handle(event, this);
	}

	getRecentEvents() {
		const recentEvents = this.recentEvents;

		this.recentEvents = [];

		return recentEvents;
	}
}

module.exports = CreditApplication;
