//@flow

class CreditInfo {
  creditValue: number;
  creditType: string;

  constructor(creditValue: number, creditType: string) {
    this.creditValue = creditValue;
    this.creditType = creditType;
  }

  static empty(): CreditInfo {
    return new CreditInfo(0, '');
  }

  static fromData(creditInfoDto: Object): CreditInfo {
    return new CreditInfo(creditInfoDto.creditValue, creditInfoDto.creditType);
  }

  isEmpty() {
    return this.creditValue === 0 && this.creditType === '';
  }

  isFilled() {
    return this.creditValue > 0 && this.creditType.length > 0;
  }
}

module.exports = CreditInfo;
