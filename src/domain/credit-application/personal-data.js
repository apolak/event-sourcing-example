//@flow

class PersonalData {
  firstName: string;
  lastName: string;
  pesel: string;
  documentId: string;
  city: string;
  phone: string;

  constructor(firstName: string, lastName: string, pesel: string, documentId: string, phone: string, city: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.pesel = pesel;
    this.documentId = documentId;
    this.phone = phone;
    this.city = city;
  }

  static empty(): PersonalData {
    return new PersonalData('', '', '', '', '', '');
  }

  static fromData(personalDataDto: Object): PersonalData {
    return new PersonalData(
      personalDataDto.firstName,
      personalDataDto.lastName,
      personalDataDto.pesel,
      personalDataDto.documentId,
      personalDataDto.phone,
      personalDataDto.city
    );
  }

  isEmpty() {
    return this.firstName === '' && this.lastName === '' && this.pesel === '' && this.documentId === '' && this.phone === '' && this.city === '';
  }

  isFilled() {
    return this.firstName.length > 0 && this.lastName.length > 0 && this.pesel.length > 0 && this.documentId.length > 0 && this.phone.length > 0 && this.city.length > 0;
  }
}

module.exports = PersonalData;
