//@flow

import uuidV4 from 'uuid/v4';
import type { Id } from '../id';

class CreditApplicationId implements Id<CreditApplicationId> {
	id: string;

	constructor(id: string) {
		if (!CreditApplicationId.isValidId(id)) {
			throw new Error(`ID must be valid uuidV4, "${id}" given`);
		}

		this.id = id;
	}

	static generate(): CreditApplicationId {
		return new CreditApplicationId(uuidV4());
	}

	static fromString(id: string): CreditApplicationId {
		return new CreditApplicationId(id);
	}

	static isValidId(id: string): boolean {
		return /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(id);
	}
}

module.exports = CreditApplicationId;