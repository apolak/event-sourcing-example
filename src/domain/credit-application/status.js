//@flow

type StatusMap = {
  CREATED: string,
  PROCEDURE_OPENED: string,
  PERSONAL_DATA_FILLED: string,
  FINANCIAL_DATA_FILLED: string,
  CREDIT_INFO_FILLED: string,
  CREDIT_HISTORY_CHECK_STARTED: string,
  CREDIT_HISTORY_CHECK_NEGATIVE: string,
  CREDIT_HISTORY_CHECK_POSITIVE: string,
  APPLICATION_REJECTED: string,
  PROCEDURE_FINISHED: string,
  FORWARDED_TO_ANALYSIS: string,
  ANALYSIS_STARTED: string,
  ANALYSIS_NEGATIVE: string,
  ANALYSIS_POSITIVE: string,
  CONTRACT_PRESENTED: string,
  CONTRACT_REJECTED: string,
  CONTRACT_ACCEPTED: string,
  READY_TO_BE_PRESENTED: string
};

class Status {
  static availableTypes: StatusMap;

  type: string;

  constructor(type: string) {
    this.type = type;
  }

  static created(): Status {
    return new Status(Status.availableTypes.CREATED);
  }

  static procedureOpened(): Status {
    return new Status(Status.availableTypes.PROCEDURE_OPENED);
  }

  static personalDataFilled(): Status {
    return new Status(Status.availableTypes.PERSONAL_DATA_FILLED);
  }

  static financialDataFilled(): Status {
    return new Status(Status.availableTypes.FINANCIAL_DATA_FILLED);
  }

  static creditInfoFilled(): Status {
    return new Status(Status.availableTypes.CREDIT_INFO_FILLED);
  }

  static creditHistoryCheckStarted(): Status {
    return new Status(Status.availableTypes.CREDIT_HISTORY_CHECK_STARTED);
  }

  static creditHistoryCheckNegative(): Status {
    return new Status(Status.availableTypes.CREDIT_HISTORY_CHECK_POSITIVE);
  }

  static creditHistoryCheckPositive(): Status {
    return new Status(Status.availableTypes.CREDIT_HISTORY_CHECK_POSITIVE);
  }

  static applicationRejected(): Status {
    return new Status(Status.availableTypes.APPLICATION_REJECTED);
  }

  static procedureFinished(): Status {
    return new Status(Status.availableTypes.PROCEDURE_FINISHED);
  }

  static forwardedToAnalysis(): Status {
    return new Status(Status.availableTypes.FORWARDED_TO_ANALYSIS);
  }

  static analysisStarted(): Status {
    return new Status(Status.availableTypes.ANALYSIS_STARTED);
  }

  static analysisNegative(): Status {
    return new Status(Status.availableTypes.ANALYSIS_NEGATIVE);
  }

  static analysisPositive(): Status {
    return new Status(Status.availableTypes.ANALYSIS_POSITIVE);
  }

  static readyToBePresented(): Status {
    return new Status(Status.availableTypes.READY_TO_BE_PRESENTED);
  }

  static contractPresented(): Status {
    return new Status(Status.availableTypes.CONTRACT_PRESENTED);
  }

  static contractAccepted(): Status {
    return new Status(Status.availableTypes.CONTRACT_ACCEPTED);
  }

  static contractRejected(): Status {
    return new Status(Status.availableTypes.CONTRACT_REJECTED);
  }

  static fromType(type: string): Status {
    if (Status.availableTypes[type] === undefined) {
      throw new Error(`Status "${type}" is not supported.`);
    }

    return new Status(type);
  }

  is(type: string): boolean {
    return this.type === type;
  }
}

Status.availableTypes = {
  CREATED: 'CREATED',
  PROCEDURE_OPENED: 'PROCEDURE_OPENED',
  PERSONAL_DATA_FILLED: 'PERSONAL_DATA_FILLED',
  FINANCIAL_DATA_FILLED: 'FINANCIAL_DATA_FILLED',
  CREDIT_INFO_FILLED: 'CREDIT_INFO_FILLED',
  CREDIT_HISTORY_CHECK_STARTED: 'CREDIT_HISTORY_CHECK_STARTED',
  CREDIT_HISTORY_CHECK_NEGATIVE: 'CREDIT_HISTORY_CHECK_NEGATIVE',
  CREDIT_HISTORY_CHECK_POSITIVE: 'CREDIT_HISTORY_CHECK_POSITIVE',
  APPLICATION_REJECTED: 'APPLICATION_REJECTED',
  PROCEDURE_FINISHED: 'PROCEDURE_FINISHED',
  FORWARDED_TO_ANALYSIS: 'FORWARDED_TO_ANALYSIS',
  ANALYSIS_STARTED: 'ANALYSIS_STARTED',
  ANALYSIS_NEGATIVE: 'ANALYSIS_NEGATIVE',
  ANALYSIS_POSITIVE: 'ANALYSIS_POSITIVE',
  CONTRACT_PRESENTED: 'CONTRACT_PRESENTED',
  CONTRACT_REJECTED: 'CONTRACT_REJECTED',
  CONTRACT_ACCEPTED: 'CONTRACT_ACCEPTED',
  READY_TO_BE_PRESENTED: 'READY_TO_BE_PRESENTED'
};

module.exports = Status;
