//@flow

class FinancialData {
  earnings: number;

  constructor(earnings: number) {
    this.earnings = earnings
  }

  static empty() {
    return new FinancialData(0);
  }

  static fromData(financialDataDto: Object): FinancialData {
    return new FinancialData(financialDataDto.earnings);
  }

  isEmpty() {
    return this.earnings === 0;
  }

  isFilled() {
    return this.earnings > 0;
  }
}

module.exports = FinancialData;
