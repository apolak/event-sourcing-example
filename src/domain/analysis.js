//@flow
import AnalysisId from './analysis/analysis-id';
import AnalysisStatus from './analysis/analysis-status';
import AnalysisType from './analysis/analysis-type';
import UserId from './user-id';
import EventsHistory from './events-history';
import Event from './event';

import eventHandlers from './handlers/event-handlers/analysis';
import type { EventHandler } from './handlers/event-handler';

import Events from './events/analysis';

type AnalysisToBePerformed = {
  type: AnalysisType,
  status: AnalysisStatus
};

class Analysis {
  analysisId: AnalysisId;
  targetId: string;
  recentEvents: Array<Event>;
  analysisToPerform: Array<AnalysisToBePerformed>;
  isActive: boolean;

  constructor(analysisId: AnalysisId) {
    this.analysisId = analysisId;
    this.recentEvents = [];

    this.analysisToPerform = [
      {
        type: AnalysisType.personalBackground(),
        status: AnalysisStatus.notPerformed()
      },
      {
        type: AnalysisType.previousCredits(),
        status: AnalysisStatus.notPerformed()
      },
      {
        type: AnalysisType.earnings(),
        status: AnalysisStatus.notPerformed()
      }
    ];

    this.isActive = true;
  }

  static reconstituteFrom(eventsHistory: EventsHistory<AnalysisId>): Analysis {
    let order = new Analysis(eventsHistory.aggregateId);

    eventsHistory.events.forEach((event: Event) => order.applyEvent(event));

    return order;
  }

  connectWithAnalysisTarget(analysisTargetId: string) {
    const connectedWithTargetEvent: Event = Events.v1.AnalysisConnectedWithTargetEvent.Instance.withoutCreationDate(this.analysisId.id, analysisTargetId);

    this.recentEvents.push(connectedWithTargetEvent);
    this.applyEvent(connectedWithTargetEvent);
  }

  markAnalysisAsPositive(analysisType: AnalysisType) {
    if (this.isActive) {
      const analysis: ?AnalysisToBePerformed = this.analysisToPerform.find((analysisToPerform) => analysisToPerform.type.is(analysisType.type));

      if (analysis) {
        const analysisPositiveEvent: Event = Events.v1.AnalysisPositiveEvent.Instance.withoutCreationDate(this.analysisId.id, analysisType.type);

        this.recentEvents.push(analysisPositiveEvent);
        this.applyEvent(analysisPositiveEvent);

        if (this.hasEndedPositive()) {
          const analysisFinishedEvent: Event = Events.v1.AnalysisFinishedEvent.Instance.withoutCreationDate(this.analysisId.id, this.targetId, true);

          this.recentEvents.push(analysisFinishedEvent);
          this.applyEvent(analysisFinishedEvent);
        }
      }
    }
  }

  markAnalysisAsNegative(analysisType: AnalysisType) {
    if (this.isActive) {
      const analysis: ?AnalysisToBePerformed = this.analysisToPerform.find((analysisToPerform) => analysisToPerform.type.is(analysisType.type));

      if (analysis) {
        const analysisNegativeEvent: Event = Events.v1.AnalysisNegativeEvent.Instance.withoutCreationDate(this.analysisId.id, analysisType.type);
        const analysisFinishedEvent: Event = Events.v1.AnalysisFinishedEvent.Instance.withoutCreationDate(this.analysisId.id, this.targetId, false);

        this.recentEvents.push(analysisNegativeEvent);
        this.recentEvents.push(analysisFinishedEvent);

        this.applyEvent(analysisNegativeEvent);
        this.applyEvent(analysisFinishedEvent);
      }
    }
  }

  hasEndedPositive(): boolean {
    return this.analysisToPerform.reduce((previous: boolean, current: AnalysisToBePerformed): boolean => {
      if (previous) {
        return current.status.is(AnalysisStatus.availableTypes.POSITIVE);
      }

      return previous;
    }, true);
  }

  applyEvent(event: Event) {
    const handler = eventHandlers.find((handler: EventHandler<Analysis>) => handler.canHandle(event));

    if (handler === undefined) {
      throw new Error(`Could not find handler for event ${event.name}`);
    }

    handler.handle(event, this);
  }

  getRecentEvents() {
    const recentEvents = this.recentEvents;

    this.recentEvents = [];

    return recentEvents;
  }
}

module.exports = Analysis;
