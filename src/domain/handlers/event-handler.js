//@flow

import Event from '../event';

interface EventHandler<T> {
	canHandle(event: Event): boolean,
	handle(event: Event, target: T): void
}

export type { EventHandler };
