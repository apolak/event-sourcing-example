//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import CreditApplication from '../../../credit-application';

import type { EventHandler } from '../../event-handler';

class AnalysisRejectedEventHandler implements EventHandler<CreditApplication> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.AnalysisRejectedEvent.eventName
      && event.version === Events.v1.AnalysisRejectedEvent.version
      && event.type === Events.v1.AnalysisRejectedEvent.type;
  }

  handle(event: Event, creditApplication: CreditApplication): void {
    creditApplication.creditApplicationStatus = Status.analysisNegative();
  }
}

module.exports = new AnalysisRejectedEventHandler();
