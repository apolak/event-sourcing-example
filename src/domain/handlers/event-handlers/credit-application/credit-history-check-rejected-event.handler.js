//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import CreditApplication from '../../../credit-application';

import type { EventHandler } from '../../event-handler';

class CreditHistoryCheckRejectedEventHandler implements EventHandler<CreditApplication> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.CreditHistoryCheckRejectedEvent.eventName
      && event.version === Events.v1.CreditHistoryCheckRejectedEvent.version
      && event.type === Events.v1.CreditHistoryCheckRejectedEvent.type;
  }

  handle(event: Event, creditApplication: CreditApplication): void {
    creditApplication.creditApplicationStatus = Status.creditHistoryCheckNegative();
  }
}

module.exports = new CreditHistoryCheckRejectedEventHandler();
