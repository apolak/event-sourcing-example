//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import PersonalData from '../../../credit-application/personal-data';
import CreditApplication from '../../../credit-application';

import type { EventHandler } from '../../event-handler';

class PersonalDataFilledEventHandler implements EventHandler<CreditApplication> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.PersonalDataFilledEvent.eventName
      && event.version === Events.v1.PersonalDataFilledEvent.version
      && event.type === Events.v1.PersonalDataFilledEvent.type;
  }

  handle(event: Event, creditApplication: CreditApplication): void {
    creditApplication.creditApplicationStatus = Status.personalDataFilled();

    creditApplication.personalData = PersonalData.fromData({
      firstName: event.payload.firstName,
      lastName: event.payload.lastName,
      pesel: event.payload.pesel,
      documentId: event.payload.documentId,
      phone: event.payload.phone,
      city: event.payload.city
    });
  }
}

module.exports = new PersonalDataFilledEventHandler();
