//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import CreditApplication from '../../../credit-application';

import type { EventHandler } from '../../event-handler';

class CreditHistoryCheckAcceptedEventHandler implements EventHandler<CreditApplication> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.CreditHistoryCheckAcceptedEvent.eventName
      && event.version === Events.v1.CreditHistoryCheckAcceptedEvent.version
      && event.type === Events.v1.CreditHistoryCheckAcceptedEvent.type;
  }

  handle(event: Event, creditApplication: CreditApplication): void {
    creditApplication.creditApplicationStatus = Status.creditHistoryCheckPositive();
  }
}

module.exports = new CreditHistoryCheckAcceptedEventHandler();
