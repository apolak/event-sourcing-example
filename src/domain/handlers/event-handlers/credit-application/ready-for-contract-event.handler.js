//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import CreditApplication from '../../../credit-application';

import type { EventHandler } from '../../event-handler';

class ReadyForContractEventHandler implements EventHandler<CreditApplication> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.ReadyForContractEvent.eventName
      && event.version === Events.v1.ReadyForContractEvent.version
      && event.type === Events.v1.ReadyForContractEvent.type;
  }

  handle(event: Event, creditApplication: CreditApplication): void {
    creditApplication.creditApplicationStatus = Status.analysisPositive();
  }
}

module.exports = new ReadyForContractEventHandler();
