//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import CreditApplication from '../../../credit-application';

import type { EventHandler } from '../../event-handler';

class AnalysisStartedEventHandler implements EventHandler<CreditApplication> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.AnalysisStartedEvent.eventName
      && event.version === Events.v1.AnalysisStartedEvent.version
      && event.type === Events.v1.AnalysisStartedEvent.type;
  }

  handle(event: Event, creditApplication: CreditApplication): void {
    creditApplication.creditApplicationStatus = Status.analysisStarted();
  }
}

module.exports = new AnalysisStartedEventHandler();
