import OpenCreditApplicationProcedureEventHandler from './open-credit-application-procedure-event.handler';
import PersonalDataFilledEVentHandler from './personal-data-filled-event.handler';
import FinancialDataFilledEventHandler from './financial-data-filled-event.handler';
import CreditInfoFilledEventHandler from './credit-info-filled-event.handler';
import CreditHistoryCheckEventHandler from './credit-history-check-event.handler';
import CreditHistoryCheckAcceptedEventHandler from './credit-history-check-accepted-event.handler';
import CreditHistoryCheckRejectedEventHandler from './credit-history-check-rejected-event.handler';
import CreditApplicationRejectedEventHandler from './credit-application-rejected-event.handler';
import AnalysisStartedEventHandler from './analysis-started-event.handler';
import AnalysisRejectedEventHandler from './analysis-rejected-event.handler';
import ReadyForContractEventHandler from './ready-for-contract-event.handler';
import ReadyForPresentationEventHandler from './ready-for-presentation-event.handler';
import ContractPresentedEventHandler from './contract-presented-event.handler';
import CloseCreditApplicationProcedureEventHandler from './close-credit-application-procedure-event.handler';
import ContractRejectedEventHandler from './contract-rejected-event.handler';
import ContractAcceptedEventHandler from './contract-accepted-event.handler';
import ForwardedToAnalysisEventHandler from './forwarded-to-analysis-event.handler';

module.exports = [
  OpenCreditApplicationProcedureEventHandler,
  PersonalDataFilledEVentHandler,
  FinancialDataFilledEventHandler,
  CreditInfoFilledEventHandler,
  CreditHistoryCheckEventHandler,
  CreditHistoryCheckAcceptedEventHandler,
  CreditHistoryCheckRejectedEventHandler,
  CreditApplicationRejectedEventHandler,
  AnalysisStartedEventHandler,
  ReadyForContractEventHandler,
  ReadyForPresentationEventHandler,
  ContractPresentedEventHandler,
  CloseCreditApplicationProcedureEventHandler,
  ContractRejectedEventHandler,
  ContractAcceptedEventHandler,
  ForwardedToAnalysisEventHandler,
  AnalysisRejectedEventHandler
];
