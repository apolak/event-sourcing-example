//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import CreditApplication from '../../../credit-application';

import type { EventHandler } from '../../event-handler';

class ForwardedToAnalysisEventHandler implements EventHandler<CreditApplication> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.ForwardedToAnalysisEvent.eventName
      && event.version === Events.v1.ForwardedToAnalysisEvent.version
      && event.type === Events.v1.ForwardedToAnalysisEvent.type;
  }

  handle(event: Event, creditApplication: CreditApplication): void {
    creditApplication.creditApplicationStatus = Status.forwardedToAnalysis();
  }
}

module.exports = new ForwardedToAnalysisEventHandler();
