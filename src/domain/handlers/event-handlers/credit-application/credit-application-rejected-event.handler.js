//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import CreditApplication from '../../../credit-application';

import type { EventHandler } from '../../event-handler';

class CreditApplicationRejectedEventHandler implements EventHandler<CreditApplication> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.CreditApplicationRejectedEvent.eventName
      && event.version === Events.v1.CreditApplicationRejectedEvent.version
      && event.type === Events.v1.CreditApplicationRejectedEvent.type;
  }

  handle(event: Event, creditApplication: CreditApplication): void {
    creditApplication.creditApplicationStatus = Status.applicationRejected();
  }
}

module.exports = new CreditApplicationRejectedEventHandler();
