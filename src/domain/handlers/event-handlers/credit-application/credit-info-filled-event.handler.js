//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import CreditInfo from '../../../credit-application/credit-info';
import CreditApplication from '../../../credit-application';

import type { EventHandler } from '../../event-handler';

class CreditInfoFilledEventHandler implements EventHandler<CreditApplication> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.CreditInfoFilledEvent.eventName
      && event.version === Events.v1.CreditInfoFilledEvent.version
      && event.type === Events.v1.CreditInfoFilledEvent.type;
  }

  handle(event: Event, creditApplication: CreditApplication): void {
    creditApplication.creditApplicationStatus = Status.creditInfoFilled();

    creditApplication.creditInfo = CreditInfo.fromData({
      creditValue: event.payload.creditValue,
      creditType: event.payload.creditType
    });
  }
}

module.exports = new CreditInfoFilledEventHandler();
