//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import CreditApplication from '../../../credit-application';
import UserId from '../../../user-id';

import type { EventHandler } from '../../event-handler';

class OpenCreditApplicationProcedureEventHandler implements EventHandler<CreditApplication> {
	canHandle(event: Event): boolean {
    return event.name === Events.v2.OpenCreditApplicationProcedureEvent.eventName
      && event.version === Events.v2.OpenCreditApplicationProcedureEvent.version
      && event.type === Events.v2.OpenCreditApplicationProcedureEvent.type;
	}

	handle(event: Event, creditApplication: CreditApplication): void {
		creditApplication.creditApplicationStatus = Status.procedureOpened();
		creditApplication.userId = UserId.fromString(event.payload.userId);
	}
}

module.exports = new OpenCreditApplicationProcedureEventHandler();
