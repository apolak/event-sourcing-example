//@flow

import Event from '../../../event';
import Events from '../../../events/credit-application';
import Status from '../../../credit-application/status';
import FinancialData from '../../../credit-application/financial-data';
import CreditApplication from '../../../credit-application';

import type { EventHandler } from '../../event-handler';

class FinanciallDataFilledEventHandler implements EventHandler<CreditApplication> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.FinancialDataFilledEvent.eventName
      && event.version === Events.v1.FinancialDataFilledEvent.version
      && event.type === Events.v1.FinancialDataFilledEvent.type;
  }

  handle(event: Event, creditApplication: CreditApplication): void {
    creditApplication.creditApplicationStatus = Status.financialDataFilled();

    creditApplication.financialData = FinancialData.fromData({
      earnings: event.payload.earnings
    });
  }
}

module.exports = new FinanciallDataFilledEventHandler();
