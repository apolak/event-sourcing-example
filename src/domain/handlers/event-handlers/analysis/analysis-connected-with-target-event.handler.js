//@flow

import Event from '../../../event';
import Events from '../../../events/analysis';
import Analysis from '../../../analysis';

import type { EventHandler } from '../../event-handler';

class AnalysisConnectedWithTargetEventHandler implements EventHandler<Analysis> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.AnalysisConnectedWithTargetEvent.eventName
      && event.version === Events.v1.AnalysisConnectedWithTargetEvent.version
      && event.type === Events.v1.AnalysisConnectedWithTargetEvent.type;
  }

  handle(event: Event, analysis: Analysis): void {
    analysis.targetId = event.payload.analysisTargetId;
  }
}

module.exports = new AnalysisConnectedWithTargetEventHandler();
