//@flow

import Event from '../../../event';
import Events from '../../../events/analysis';
import Analysis from '../../../analysis';
import AnalysisStatus from '../../../analysis/analysis-status';

import type { EventHandler } from '../../event-handler';

class AnalysisPositiveEventHandler implements EventHandler<Analysis> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.AnalysisPositiveEvent.eventName
      && event.version === Events.v1.AnalysisPositiveEvent.version
      && event.type === Events.v1.AnalysisPositiveEvent.type;
  }

  handle(event: Event, analysis: Analysis): void {
    analysis.analysisToPerform =  analysis.analysisToPerform.map((analysisToPerform) => {
      if (analysisToPerform.type.is(event.payload.type)) {
        return {
          type: analysisToPerform.type,
          status: AnalysisStatus.positive()
        };
      }

      return analysisToPerform;
    });
  }
}

module.exports = new AnalysisPositiveEventHandler();
