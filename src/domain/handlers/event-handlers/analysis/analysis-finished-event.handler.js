//@flow

import Event from '../../../event';
import Events from '../../../events/analysis';
import Analysis from '../../../analysis';

import type { EventHandler } from '../../event-handler';

class AnalysisFinishedEventHandler implements EventHandler<Analysis> {
  canHandle(event: Event): boolean {
    return event.name === Events.v1.AnalysisFinishedEvent.eventName
      && event.version === Events.v1.AnalysisFinishedEvent.version
      && event.type === Events.v1.AnalysisNegativeEvent.type;
  }

  handle(event: Event, analysis: Analysis): void {
    analysis.isActive = false;
  }
}

module.exports = new AnalysisFinishedEventHandler();
