import AnalysisPositiveEventHandler from './analysis-positive-event.handler';
import AnalysisNegativeEventHandler from './analysis-negative-event.handler';
import AnalysisFinishedEventHandler from './analysis-finished-event.handler';
import AnalysisConnectedWithTargetEventHandler from './analysis-connected-with-target-event.handler';

module.exports = [
  AnalysisPositiveEventHandler,
  AnalysisNegativeEventHandler,
  AnalysisFinishedEventHandler,
  AnalysisConnectedWithTargetEventHandler
];
