import * as OpenCreditApplicationProcedureV1 from './v1/open-credit-application-procedure.event';
import * as OpenCreditApplicationProcedureV2 from './v2/open-credit-application-procedure.event';
import * as PersonalDataFilledEvent from './v1/personal-data-filled.event';
import * as FinancialDataFilledEvent from './v1/financial-data-filled.event';
import * as CreditInfoFilledEvent from './v1/credit-info-filled.event';
import * as CreditHistoryCheckEvent from './v1/credit-history-check.event';
import * as CreditHistoryCheckAcceptedEvent from './v1/credit-history-check-accepted.event';
import * as CreditHistoryCheckRejectedEvent from './v1/credit-history-check-rejected.event';
import * as CreditApplicationRejectedEvent from './v1/credit-application-rejected.event';
import * as AnalysisStartedEvent from './v1/analysis-started.event';
import * as AnalysisRejectedEvent from './v1/analysis-rejected.event';
import * as ForwardedToAnalysisEvent from './v1/forwarded-to-analysis.event';
import * as ReadyForContractEvent from './v1/ready-for-contract.event';
import * as ReadyForPresentationEvent from './v1/ready-for-presentation.event';
import * as ContractPresentedEvent from './v1/contract-presented.event';
import * as CloseCreditApplicationProcedureEvent from './v1/close-credit-application-procedure.event';
import * as ContractAcceptedEvent from './v1/contract-accepted.event';
import * as ContractRejectedEvent from './v1/contract-rejected.event';

module.exports = {
	v1: {
		OpenCreditApplicationProcedureEvent: OpenCreditApplicationProcedureV1,
    PersonalDataFilledEvent: PersonalDataFilledEvent,
    FinancialDataFilledEvent: FinancialDataFilledEvent,
    CreditInfoFilledEvent: CreditInfoFilledEvent,
    CreditHistoryCheckEvent: CreditHistoryCheckEvent,
    CreditHistoryCheckAcceptedEvent: CreditHistoryCheckAcceptedEvent,
    CreditHistoryCheckRejectedEvent: CreditHistoryCheckRejectedEvent,
    CreditApplicationRejectedEvent: CreditApplicationRejectedEvent,
    ForwardedToAnalysisEvent: ForwardedToAnalysisEvent,
    AnalysisStartedEvent: AnalysisStartedEvent,
    ReadyForContractEvent: ReadyForContractEvent,
    ReadyForPresentationEvent: ReadyForPresentationEvent,
    ContractPresentedEvent: ContractPresentedEvent,
    CloseCreditApplicationProcedureEvent: CloseCreditApplicationProcedureEvent,
    ContractAcceptedEvent: ContractAcceptedEvent,
    ContractRejectedEvent: ContractRejectedEvent,
    AnalysisRejectedEvent: AnalysisRejectedEvent
	},
	v2: {
    OpenCreditApplicationProcedureEvent: OpenCreditApplicationProcedureV2
	},
	v3: {
	}
};
