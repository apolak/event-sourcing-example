//@flow

import Event from '../../../event';

const NAME = 'OpenCreditApplicationProcedureEvent';
const VERSION = 2;
const TYPE = 'CreditApplication';

class OpenCreditApplicationProcedureEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, userId: string) {
    super(creditApplicationId, creationDate, {
      userId: userId
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, userId: string) {
    return new OpenCreditApplicationProcedureEvent(creditApplicationId, new Date().toISOString(), userId);
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new OpenCreditApplicationProcedureEvent(creditApplicationId, creationDate, payload.userId);
  }
}

module.exports.Instance = OpenCreditApplicationProcedureEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
