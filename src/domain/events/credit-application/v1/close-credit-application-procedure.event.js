//@flow

import Event from '../../../event';

const NAME = 'CloseCreditApplicationProcedureEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class CloseCreditApplicationProcedureEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, result: string) {
    super(creditApplicationId, creationDate, {
      result: result
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, result: string) {
    return new CloseCreditApplicationProcedureEvent(creditApplicationId, new Date().toISOString(), result);
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new CloseCreditApplicationProcedureEvent(creditApplicationId, creationDate, payload.result);
  }
}

module.exports.Instance = CloseCreditApplicationProcedureEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
