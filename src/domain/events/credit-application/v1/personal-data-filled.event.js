//@flow

import Event from '../../../event';
import PersonalData from '../../../credit-application/personal-data';

const NAME = 'PersonalDataFilledEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class PersonalDataFilledEvent extends Event {
  constructor(
    creditApplicationId: string,
    creationDate: string,
    firstName: string,
    lastName: string,
    pesel: string,
    documentId: string,
    phone: string,
    city: string
  ) {
    super(creditApplicationId, creationDate, {
      firstName,
      lastName,
      pesel,
      documentId,
      phone,
      city
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, personalData: PersonalData) {
    return new PersonalDataFilledEvent(
      creditApplicationId,
      new Date().toISOString(),
      personalData.firstName,
      personalData.lastName,
      personalData.pesel,
      personalData.documentId,
      personalData.phone,
      personalData.city
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new PersonalDataFilledEvent(
      creditApplicationId,
      creationDate,
      payload.firstName,
      payload.lastName,
      payload.pesel,
      payload.documentId,
      payload.phone,
      payload.city
    );
  }
}

module.exports.Instance = PersonalDataFilledEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
