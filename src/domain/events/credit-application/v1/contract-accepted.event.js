//@flow

import Event from '../../../event';

const NAME = 'ContractAcceptedEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class ContractAcceptedEvent extends Event {
  constructor(contractId: string, creationDate: string) {
    super(contractId, creationDate, {});

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(contractId: string) {
    return new ContractAcceptedEvent(
      contractId,
      new Date().toISOString()
    );
  }

  static fromPayload(contractId: string, creationDate: string, payload: Object) {
    return new ContractAcceptedEvent(
      contractId,
      creationDate
    );
  }
}

module.exports.Instance = ContractAcceptedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
