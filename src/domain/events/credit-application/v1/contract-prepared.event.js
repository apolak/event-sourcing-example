//@flow

import Event from '../../../event';

const NAME = 'ContractPreparedEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class ContractPreparedEvent extends Event {
  constructor(contractId: string, creationDate: string, targetId: string, targetType: string) {
    super(contractId, creationDate, {
      targetId,
      targetType
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(contractId: string, targetId: string, targetType: string) {
    return new ContractPreparedEvent(
      contractId,
      new Date().toISOString(),
      targetId,
      targetType
    );
  }

  static fromPayload(contractId: string, creationDate: string, payload: Object) {
    return new ContractPreparedEvent(
      contractId,
      creationDate,
      payload.targetId,
      payload.targetType
    );
  }
}

module.exports.Instance = ContractPreparedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
