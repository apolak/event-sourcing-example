//@flow

import Event from '../../../event';

const NAME = 'CreditHistoryCheckEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class CreditHistoryCheckEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, pesel: string) {
    super(creditApplicationId, creationDate, {
      pesel: pesel
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, pesel: string) {
    return new CreditHistoryCheckEvent(creditApplicationId, new Date().toISOString(), pesel);
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new CreditHistoryCheckEvent(creditApplicationId, creationDate, payload.pesel);
  }
}

module.exports.Instance = CreditHistoryCheckEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
