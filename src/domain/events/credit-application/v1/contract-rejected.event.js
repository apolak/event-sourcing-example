//@flow

import Event from '../../../event';

const NAME = 'ContractRejectedEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class ContractRejectedEvent extends Event {
  constructor(contractId: string, creationDate: string, reason: string) {
    super(contractId, creationDate, {
      reason
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(contractId: string, reason: string) {
    return new ContractRejectedEvent(
      contractId,
      new Date().toISOString(),
      reason
    );
  }

  static fromPayload(contractId: string, creationDate: string, payload: Object) {
    return new ContractRejectedEvent(
      contractId,
      creationDate,
      payload.reason
    );
  }
}

module.exports.Instance = ContractRejectedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
