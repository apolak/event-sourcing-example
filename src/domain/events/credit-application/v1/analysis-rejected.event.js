//@flow

import Event from '../../../event';

const NAME = 'AnalysisRejectedEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class AnalysisRejectedEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, reason: string) {
    super(creditApplicationId, creationDate, {
      reason
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, reason: string) {
    return new AnalysisRejectedEvent(
      creditApplicationId,
      new Date().toISOString(),
      reason
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new AnalysisRejectedEvent(
      creditApplicationId,
      creationDate,
      payload.reason
    );
  }
}

module.exports.Instance = AnalysisRejectedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
