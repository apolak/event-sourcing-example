//@flow

import Event from '../../../event';

const NAME = 'CreditHistoryCheckRejectedEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class CreditHistoryCheckRejectedEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, reason: string) {
    super(creditApplicationId, creationDate, {
      reason
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, reason: string) {
    return new CreditHistoryCheckRejectedEvent(
      creditApplicationId,
      new Date().toISOString(),
      reason
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new CreditHistoryCheckRejectedEvent(
      creditApplicationId,
      creationDate,
      payload.reason
    );
  }
}

module.exports.Instance = CreditHistoryCheckRejectedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
