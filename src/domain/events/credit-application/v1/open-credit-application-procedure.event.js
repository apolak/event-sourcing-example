//@flow

import Event from '../../../event';

const NAME = 'OpenCreditApplicationProcedureEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class OpenCreditApplicationProcedureEvent extends Event {
	constructor(creditApplicationId: string, creationDate: string) {
		super(creditApplicationId, creationDate, {});

		this.name = NAME;
		this.version = VERSION;
    this.type = TYPE;
	}

	static withoutCreationDate(creditApplicationId: string) {
		return new OpenCreditApplicationProcedureEvent(creditApplicationId, new Date().toISOString());
	}

	static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
		return new OpenCreditApplicationProcedureEvent(creditApplicationId, creationDate);
	}
}

module.exports.Instance = OpenCreditApplicationProcedureEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
