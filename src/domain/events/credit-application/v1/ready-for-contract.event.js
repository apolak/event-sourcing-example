//@flow

import Event from '../../../event';

const NAME = 'ReadyForContractEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class ReadyForContractEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string) {
    super(creditApplicationId, creationDate, {});

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string) {
    return new ReadyForContractEvent(
      creditApplicationId,
      new Date().toISOString()
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new ReadyForContractEvent(
      creditApplicationId,
      creationDate
    );
  }
}

module.exports.Instance = ReadyForContractEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
