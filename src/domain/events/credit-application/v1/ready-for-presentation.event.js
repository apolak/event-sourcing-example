//@flow

import Event from '../../../event';

const NAME = 'ReadyForPresentationEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class ReadyForPresentationEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, contractId: string) {
    super(creditApplicationId, creationDate, {
      contractId
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, contractId: string) {
    return new ReadyForPresentationEvent(
      creditApplicationId,
      new Date().toISOString(),
      contractId
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new ReadyForPresentationEvent(
      creditApplicationId,
      creationDate,
      payload.contractId
    );
  }
}

module.exports.Instance = ReadyForPresentationEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
