//@flow

import Event from '../../../event';

const NAME = 'CreditApplicationRejectedEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class CreditApplicationRejectedEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, reason: string) {
    super(creditApplicationId, creationDate, {
      reason
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, reason: string) {
    return new CreditApplicationRejectedEvent(
      creditApplicationId,
      new Date().toISOString(),
      reason
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new CreditApplicationRejectedEvent(
      creditApplicationId,
      creationDate,
      payload.reason
    );
  }
}

module.exports.Instance = CreditApplicationRejectedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
