//@flow

import Event from '../../../event';
import CreditInfo from '../../../credit-application/credit-info';

const NAME = 'CreditInfoFilledEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class CreditInfoFilledEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, creditValue: number, creditType: string) {
    super(creditApplicationId, creationDate, {
      creditValue,
      creditType
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, creditInfo: CreditInfo) {
    return new CreditInfoFilledEvent(
      creditApplicationId,
      new Date().toISOString(),
      creditInfo.creditValue,
      creditInfo.creditType
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new CreditInfoFilledEvent(
      creditApplicationId,
      creationDate,
      payload.creditValue,
      payload.creditType
    );
  }
}

module.exports.Instance = CreditInfoFilledEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
