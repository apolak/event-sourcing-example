//@flow

import Event from '../../../event';

const NAME = 'ContractPresentedEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class ContractPresentedEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, nextContactDate: string) {
    super(creditApplicationId, creationDate, {
      nextContactDate: nextContactDate
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string) {
    const nextContactDate: Date = new Date();
    nextContactDate.setDate(nextContactDate.getDate() + 7);

    return new ContractPresentedEvent(
      creditApplicationId,
      new Date().toISOString(),
      nextContactDate.toISOString()
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new ContractPresentedEvent(
      creditApplicationId,
      creationDate,
      payload.nextContactDate
    );
  }
}

module.exports.Instance = ContractPresentedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
