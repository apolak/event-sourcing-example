//@flow

import Event from '../../../event';
import FinancialData from '../../../credit-application/financial-data';

const NAME = 'FinancialDataFilledEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class FinancialDataFilledEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, earnings: number) {
    super(creditApplicationId, creationDate, {
      earnings
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, financialData: FinancialData) {
    return new FinancialDataFilledEvent(
      creditApplicationId,
      new Date().toISOString(),
      financialData.earnings
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new FinancialDataFilledEvent(
      creditApplicationId,
      creationDate,
      payload.earnings
    );
  }
}

module.exports.Instance = FinancialDataFilledEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
