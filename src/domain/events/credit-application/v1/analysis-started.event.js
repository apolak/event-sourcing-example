//@flow

import Event from '../../../event';

const NAME = 'AnalysisStartedEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class AnalysisStartedEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string, analysisId: string) {
    super(creditApplicationId, creationDate, {
      analysisId
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string, analysisId: string) {
    return new AnalysisStartedEvent(
      creditApplicationId,
      new Date().toISOString(),
      analysisId
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new AnalysisStartedEvent(
      creditApplicationId,
      creationDate,
      payload.analysisId
    );
  }
}

module.exports.Instance = AnalysisStartedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
