//@flow

import Event from '../../../event';

const NAME = 'CreditHistoryCheckAcceptedEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class CreditHistoryCheckAcceptedEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string) {
    super(creditApplicationId, creationDate, {});

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string) {
    return new CreditHistoryCheckAcceptedEvent(
      creditApplicationId,
      new Date().toISOString()
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new CreditHistoryCheckAcceptedEvent(
      creditApplicationId,
      creationDate
    );
  }
}

module.exports.Instance = CreditHistoryCheckAcceptedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
