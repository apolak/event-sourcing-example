//@flow

import Event from '../../../event';

const NAME = 'ForwardedToAnalysisEvent';
const VERSION = 1;
const TYPE = 'CreditApplication';

class ForwardedToAnalysisEvent extends Event {
  constructor(creditApplicationId: string, creationDate: string) {
    super(creditApplicationId, creationDate, {});

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(creditApplicationId: string) {
    return new ForwardedToAnalysisEvent(
      creditApplicationId,
      new Date().toISOString()
    );
  }

  static fromPayload(creditApplicationId: string, creationDate: string, payload: Object) {
    return new ForwardedToAnalysisEvent(
      creditApplicationId,
      creationDate
    );
  }
}

module.exports.Instance = ForwardedToAnalysisEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
