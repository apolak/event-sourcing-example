import AnalysisEvents from './analysis';
import CreditApplicationEvents from './credit-application';

module.exports = {
  Analysis: AnalysisEvents,
  CreditApplication: CreditApplicationEvents
};
