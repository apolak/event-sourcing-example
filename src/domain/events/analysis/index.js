import * as AnalysisNegativeEvent from './v1/analysis-negative.event';
import * as AnalysisPositiveEvent from './v1/analysis-positive.event';
import * as AnalysisFinishedEvent from './v1/analysis-finished.event';
import * as AnalysisConnectedWithTargetEvent from './v1/analysis-connected-with-target.event';

module.exports = {
  v1: {
    AnalysisNegativeEvent: AnalysisNegativeEvent,
    AnalysisPositiveEvent: AnalysisPositiveEvent,
    AnalysisFinishedEvent: AnalysisFinishedEvent,
    AnalysisConnectedWithTargetEvent: AnalysisConnectedWithTargetEvent
  },
  v2: {
  },
  v3: {
  }
};
