//@flow

import Event from '../../../event';

const NAME = 'AnalysisConnectedWithTargetEvent';
const VERSION = 1;
const TYPE = 'Analysis';

class AnalysisConnectedWithTargetEvent extends Event {
  constructor(analysisId: string, creationDate: string, analysisTargetId: string) {
    super(analysisId, creationDate, {
      analysisTargetId
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(analysisId: string, analysisTargetId: string) {
    return new AnalysisConnectedWithTargetEvent(
      analysisId,
      new Date().toISOString(),
      analysisTargetId
    );
  }

  static fromPayload(analysisId: string, creationDate: string, payload: Object) {
    return new AnalysisConnectedWithTargetEvent(
      analysisId,
      creationDate,
      payload.analysisTargetId
    );
  }
}

module.exports.Instance = AnalysisConnectedWithTargetEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
