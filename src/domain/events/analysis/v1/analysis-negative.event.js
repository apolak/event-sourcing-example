//@flow

import Event from '../../../event';

const NAME = 'AnalysisNegativeEvent';
const VERSION = 1;
const TYPE = 'Analysis';

class AnalysisNegativeEvent extends Event {
  constructor(analysisId: string, creationDate: string, type: string) {
    super(analysisId, creationDate, {
      type
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(analysisId: string, type: string) {
    return new AnalysisNegativeEvent(
      analysisId,
      new Date().toISOString(),
      type
    );
  }

  static fromPayload(analysisId: string, creationDate: string, payload: Object) {
    return new AnalysisNegativeEvent(
      analysisId,
      creationDate,
      payload.type
    );
  }
}

module.exports.Instance = AnalysisNegativeEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
