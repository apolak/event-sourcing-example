//@flow

import Event from '../../../event';

const NAME = 'AnalysisFinishedEvent';
const VERSION = 1;
const TYPE = 'Analysis';

class AnalysisFinishedEvent extends Event {
  constructor(analysisId: string, creationDate: string, analysisTargetId: string, result: boolean) {
    super(analysisId, creationDate, {
      analysisTargetId,
      result
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(analysisId: string, analysisTargetId: string, result: boolean) {
    return new AnalysisFinishedEvent(
      analysisId,
      new Date().toISOString(),
      analysisTargetId,
      result
    );
  }

  static fromPayload(analysisId: string, creationDate: string, payload: Object) {
    return new AnalysisFinishedEvent(
      analysisId,
      creationDate,
      payload.analysisTargetId,
      payload.result
    );
  }
}

module.exports.Instance = AnalysisFinishedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
