//@flow

import Event from '../../../event';

const NAME = 'AnalysisPositiveEvent';
const VERSION = 1;
const TYPE = 'Analysis';

class AnalysisPositiveEvent extends Event {
  constructor(analysisId: string, creationDate: string, type: string) {
    super(analysisId, creationDate, {
      type
    });

    this.name = NAME;
    this.version = VERSION;
    this.type = TYPE;
  }

  static withoutCreationDate(analysisId: string, type: string) {
    return new AnalysisPositiveEvent(
      analysisId,
      new Date().toISOString(),
      type
    );
  }

  static fromPayload(analysisId: string, creationDate: string, payload: Object) {
    return new AnalysisPositiveEvent(
      analysisId,
      creationDate,
      payload.type
    );
  }
}

module.exports.Instance = AnalysisPositiveEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
module.exports.type = TYPE;
