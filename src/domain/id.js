//@flow
interface Id<T> {
	id: string,
	static generate(): T,
	static fromString(id: string): T,
	static isValidId(id: string): boolean
}

export type { Id };