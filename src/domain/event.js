//@flow

class Event {
	id: string;
	payload: Object;
	name: string;
	version: number;
	creationDate: string;
	type: string;

	constructor(aggregateId: string, creationDate: string, payload: Object) {
		this.id = aggregateId;
		this.payload = payload;

		this.name = 'BaseEvent';
		this.version = 1;
		this.type = 'BaseType';
		this.creationDate = creationDate;
	}
}

module.exports = Event;
