//@flow

import uuidV4 from 'uuid/v4';
import type { Id } from './id';

class UserId implements Id<UserId> {
	id: string;

	constructor(id: string) {
		if (!UserId.isValidId(id)) {
			throw new Error(`ID must be valid uuidV4, "${id}" given`);
		}

		this.id = id;
	}

	static generate(): UserId {
		return new UserId(uuidV4());
	}

	static fromString(id: string): UserId {
		return new UserId(id);
	}

	static unknownUser(): UserId {
		return new UserId(uuidV4({
      random: [
        0x10, 0x91, 0x56, 0xbe, 0xc4, 0xfb, 0xc1, 0xea,
        0x71, 0xb4, 0xef, 0xe1, 0x67, 0x1c, 0x58, 0x36
      ]
    }));
	}

	static isValidId(id: string): boolean {
		return /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(id);
	}
}

module.exports = UserId;
