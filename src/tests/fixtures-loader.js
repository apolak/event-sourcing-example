import CreditApplicationBuilder from './credit-application-builder';

class FixturesLoader {
  constructor(eventStore) {
    this.eventStore = eventStore
  }

  build() {
    const creditApplicationBuilder = new CreditApplicationBuilder(this.eventStore);

    const availableTypes = [
      'default',
      'closed-positive',
      'closed-negative',
      'during-analysis',
      'contract-rejected'
    ];

    for (let i = 0; i < 500; i++) {
      const type = availableTypes[Math.floor(Math.random() * availableTypes.length)];

      creditApplicationBuilder.build(type);
    }
  }
}

module.exports = FixturesLoader;
