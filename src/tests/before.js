import fs from 'fs';

module.exports = function(csvPath) {
  beforeEach(function() {
    if (fs.existsSync(csvPath)) {
      fs.unlinkSync(csvPath);
    }
  });

  afterEach(function() {
    if (fs.existsSync(csvPath)) {
      fs.unlinkSync(csvPath);
    }
  });
};
