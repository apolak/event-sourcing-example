import AnalysisId from '../../domain/analysis/analysis-id';
import PersonalData from '../../domain/credit-application/personal-data';
import FinancialData from '../../domain/credit-application/financial-data';
import CreditInfo from '../../domain/credit-application/credit-info';
import Events from '../../domain/events/credit-application';

class CreditApplicationEventsBuilder {
  constructor(id) {
    this.creditApplicationId = id;
    this.events = [];
  }

  withOpenProcedureEvent(version = '1') {
    this.events.push(
      Events[`v${version}`].OpenCreditApplicationProcedureEvent.Instance.withoutCreationDate(this.creditApplicationId.id),
    )

    return this;
  }

  withPersonalDataFilledEvent(version = '1', personalData = PersonalData.fromData({
    firstName: 'John',
    lastName: 'Doe',
    pesel: '123123123',
    documentId: 'AHD123123',
    phone: '123456123',
    city: 'Warsaw'
  })) {
    this.events.push(
      Events[`v${version}`].PersonalDataFilledEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        personalData
      ),
    )

    return this;
  }

  withFinancialDataFilledEvent(version = '1', financialData = FinancialData.fromData({
    earnings: 6000
  })) {
    this.events.push(
      Events[`v${version}`].FinancialDataFilledEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        financialData
      ),
    )

    return this;
  }

  withCreditInfoFilledEvent(version = '1', creditInfo = CreditInfo.fromData({
    creditValue: 6000,
    creditType: 'some-credit'
  })) {
    this.events.push(
      Events[`v${version}`].CreditInfoFilledEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        creditInfo
      ),
    )

    return this;
  }
  withCreditHistoryCheckEvent(version = '1', pesel = '123123123') {
    this.events.push(
      Events[`v${version}`].CreditHistoryCheckEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        pesel
      ),
    )

    return this;
  }

  withForwardedToAnalysisEvent(version = '1') {
    this.events.push(
      Events[`v${version}`].ForwardedToAnalysisEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id
      ),
    )

    return this;
  }

  withAnalysisStartedEvent(version = '1', analysisId = AnalysisId.generate()) {
    this.events.push(
      Events[`v${version}`].AnalysisStartedEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        analysisId
      ),
    )

    return this;
  }

  withReadyForContractEvent(version = '1') {
    this.events.push(
      Events[`v${version}`].ReadyForContractEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id
      ),
    )

    return this;
  }

  withReadyForPresentationEvent(version = '1') {
    this.events.push(
      Events[`v${version}`].ReadyForContractEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id
      ),
    )

    return this;
  }

  withContractPresentedEvent(version = '1') {
    this.events.push(
      Events[`v${version}`].ContractPresentedEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id
      ),
    )

    return this;
  }

  withContractAcceptedEvent(version = '1') {
    this.events.push(
      Events[`v${version}`].ContractAcceptedEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id
      ),
    )

    return this;
  }

  withContractRejectedEvent(version = '1') {
    this.events.push(
      Events[`v${version}`].ContractRejectedEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id
      ),
    )

    return this;
  }

  withCloseProcedureEvent(version = '1', result = 'success') {
    this.events.push(
      Events[`v${version}`].CloseCreditApplicationProcedureEvent.Instance.withoutCreationDate(
        this.creditApplicationId.id,
        result
      ),
    )

    return this;
  }

  getEvents() {
    return this.events;
  }
}

module.exports = CreditApplicationEventsBuilder;
