import CreditApplicationId from '../../domain/credit-application/credit-application-id';
import Events from '../../domain/events/analysis';

class AnalysisEventsBuilder {
  constructor(id) {
    this.analysisId = id;
    this.events = [];
  }

  withConnectedWithTarget(Eventversion = '1', targetId = CreditApplicationId.generate()) {
    this.events.push(
      Events[`v${version}`].AnalysisConnectedWithTargetEvent.Instance.withoutCreationDate(this.analysisId.id, targetId),
    )

    return this;
  }

  withAnalysisPositiveEvent(version = '1', type) {
    this.events.push(
      Events[`v${version}`].AnalysisPositiveEvent.Instance.withoutCreationDate(this.analysisId.id, type),
    )

    return this;
  }

  withAnalysisNegativeEvent(version = '1', type) {
    this.events.push(
      Events[`v${version}`].AnalysisNegativeEvent.Instance.withoutCreationDate(this.analysisId.id, type),
    )

    return this;
  }

  withAnalysisFinishedEvent(version = '1', targetId = CreditApplicationId.generate(), result = true) {
    this.events.push(
      Events[`v${version}`].AnalysisFinishedEvent.Instance.withoutCreationDate(this.analysisId.id, targetId, result),
    )

    return this;
  }

  getEvents() {
    return this.events;
  }
}

module.exports = AnalysisEventsBuilder;
