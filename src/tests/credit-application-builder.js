import Events from '../domain/events/credit-application';

import CreditApplicationEventsBuilder from './events-builder/credit-application-events-builder';
import AnalysisEventsBuilder from './events-builder/analysis-events-builder';

import CreditApplicationId from '../domain/credit-application/credit-application-id';
import AnalysisId from '../domain/analysis/analysis-id';

import AnalysisType from '../domain/analysis/analysis-type';

import PersonalData from '../domain/credit-application/personal-data';
import FinancialData from '../domain/credit-application/financial-data';
import CreditInfo from '../domain/credit-application/credit-info';

import faker from 'faker';

class CreditApplicationBuilder {
  constructor(eventStore) {
    this.eventStore = eventStore;
  }

  randomPersonalData() {
    return PersonalData.fromData({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      pesel: faker.random.alphaNumeric(20),
      documentId: faker.random.alphaNumeric(16),
      phone: faker.phone.phoneNumber(),
      city: faker.address.city()
    })
  }

  randomFinancialData() {
    return FinancialData.fromData({
      earnings: faker.random.number({
        min: 1000,
        max: 50000
      })
    })
  }

  randomCreditInfo() {
    return CreditInfo.fromData({
      creditValue: faker.random.number({
        min: 1000,
        max: 20000
      }),
      creditType: 'consumer'
    })
  }

  buildClosedNegative() {
    const creditApplicationId = CreditApplicationId.generate();

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withOpenProcedureEvent()
        .withPersonalDataFilledEvent(1, this.randomPersonalData())
        .withFinancialDataFilledEvent(1, this.randomFinancialData())
        .withCreditInfoFilledEvent(1, this.randomCreditInfo())
        .withCreditHistoryCheckEvent()
        .getEvents()
    );

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withForwardedToAnalysisEvent()
        .getEvents()
    );

    const analysisStartedEvent = this.eventStore.findEventOfTypeFor(creditApplicationId.id, Events.v1.AnalysisStartedEvent.eventName);
    const analysisId = AnalysisId.fromString(analysisStartedEvent.payload.analysisId);

    this.eventStore.push(
      new AnalysisEventsBuilder(analysisId)
        .withAnalysisNegativeEvent(1, AnalysisType.personalBackground().type)
        .getEvents()
    );
  }

  buildOpened() {
    const creditApplicationId = CreditApplicationId.generate();

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withOpenProcedureEvent()
        .withPersonalDataFilledEvent(1, this.randomPersonalData())
        .withFinancialDataFilledEvent(1, this.randomFinancialData())
        .withCreditInfoFilledEvent(1, this.randomCreditInfo())
        .withCreditHistoryCheckEvent()
        .getEvents()
    );
  }

  buildClosedPositive() {
    const creditApplicationId = CreditApplicationId.generate();

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withOpenProcedureEvent()
        .withPersonalDataFilledEvent(1, this.randomPersonalData())
        .withFinancialDataFilledEvent(1, this.randomFinancialData())
        .withCreditInfoFilledEvent(1, this.randomCreditInfo())
        .withCreditHistoryCheckEvent()
        .getEvents()
    );

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withForwardedToAnalysisEvent()
        .getEvents()
    );

    const analysisStartedEvent = this.eventStore.findEventOfTypeFor(creditApplicationId.id, Events.v1.AnalysisStartedEvent.eventName);
    const analysisId = AnalysisId.fromString(analysisStartedEvent.payload.analysisId);

    this.eventStore.push(
      new AnalysisEventsBuilder(analysisId)
        .withAnalysisPositiveEvent(1, AnalysisType.personalBackground().type)
        .withAnalysisPositiveEvent(1, AnalysisType.previousCredits().type)
        .withAnalysisPositiveEvent(1, AnalysisType.earnings().type)
        .withAnalysisFinishedEvent(1, creditApplicationId.id, true)
        .getEvents()
    );

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withContractPresentedEvent()
        .withContractAcceptedEvent()
        .withCloseProcedureEvent()
        .getEvents()
    );
  }

  buildContractRejected() {
    const creditApplicationId = CreditApplicationId.generate();

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withOpenProcedureEvent()
        .withPersonalDataFilledEvent(1, this.randomPersonalData())
        .withFinancialDataFilledEvent(1, this.randomFinancialData())
        .withCreditInfoFilledEvent(1, this.randomCreditInfo())
        .withCreditHistoryCheckEvent()
        .getEvents()
    );

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withForwardedToAnalysisEvent()
        .getEvents()
    );

    const analysisStartedEvent = this.eventStore.findEventOfTypeFor(creditApplicationId.id, Events.v1.AnalysisStartedEvent.eventName);
    const analysisId = AnalysisId.fromString(analysisStartedEvent.payload.analysisId);

    this.eventStore.push(
      new AnalysisEventsBuilder(analysisId)
        .withAnalysisPositiveEvent(1, AnalysisType.personalBackground().type)
        .withAnalysisPositiveEvent(1, AnalysisType.previousCredits().type)
        .withAnalysisPositiveEvent(1, AnalysisType.earnings().type)
        .withAnalysisFinishedEvent(1, creditApplicationId.id, true)
        .getEvents()
    );

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withContractPresentedEvent()
        .withContractRejectedEvent()
        .withCloseProcedureEvent(1, 'failure')
        .getEvents()
    );
  }

  buildDuringAnalysis() {
    const creditApplicationId = CreditApplicationId.generate();

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withOpenProcedureEvent()
        .withPersonalDataFilledEvent(1, this.randomPersonalData())
        .withFinancialDataFilledEvent(1, this.randomFinancialData())
        .withCreditInfoFilledEvent(1, this.randomCreditInfo())
        .withCreditHistoryCheckEvent()
        .getEvents()
    );

    this.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withForwardedToAnalysisEvent()
        .getEvents()
    );

    const analysisStartedEvent = this.eventStore.findEventOfTypeFor(creditApplicationId.id, Events.v1.AnalysisStartedEvent.eventName);
    const analysisId = AnalysisId.fromString(analysisStartedEvent.payload.analysisId);

    this.eventStore.push(
      new AnalysisEventsBuilder(analysisId)
        .withAnalysisPositiveEvent(1, AnalysisType.personalBackground().type)
        .withAnalysisPositiveEvent(1, AnalysisType.previousCredits().type)
        .getEvents()
    );
  }

  build(type) {
    switch(type) {
      case 'closed-positive':
        this.buildClosedPositive();
        break;
      case 'closed-negative':
        this.buildClosedNegative();
        break;
      case 'during-analysis':
        this.buildDuringAnalysis();
        break;
      case 'contract-rejected':
        this.buildContractRejected();
        break;
      default:
        this.buildOpened();
        break;
    }
  }
}

module.exports = CreditApplicationBuilder;
