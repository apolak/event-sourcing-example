import Event from '../domain/event';

interface Upcaster {
	canUpcast(from: Event): boolean;
	upcast(from: Event): Event;
}

export type { Upcaster };
