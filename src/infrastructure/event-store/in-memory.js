//@flow

import Event from '../../domain/event';
import Events from '../../domain/events';
import EventBus from '../event-bus';
import Upcasters from '../upcasters';

class InMemoryEventStore {
	events: Array<Object>;
	eventBus: EventBus;
	upcasters: Upcasters;

	constructor(eventBus: EventBus, upcasters: Upcasters) {
		this.events = [];
		this.eventBus = eventBus;
		this.upcasters = upcasters;
	}

	push(events: Array<Event>) {
		events.forEach((event: Event) => {
			this.events.push({
				aggregateId: event.id,
        type: event.type,
				creationDate: event.creationDate,
				name: event.name,
				version: event.version,
				payload: event.payload
			});

			this.eventBus.publish(event);
		});
	}

  findEventOfTypeFor(aggregateId: string, eventName: string): Event|null {
		const storeEvent: ?Object = this.events.find((event) => event.name === eventName && event.aggregateId === aggregateId);

		if (storeEvent) {
			return this.upcasters.upcast(Events[storeEvent.type][`v${storeEvent.version}`][storeEvent.name].Instance.fromPayload(storeEvent.aggregateId, storeEvent.creationDate, storeEvent.payload))
		}

		return null;
  }

	getAll(): Array<Event> {
		return this.events
			.map((event) => Events[event.type][`v${event.version}`][event.name].Instance.fromPayload(event.aggregateId, event.creationDate, event.payload))
			.map((event) => this.upcasters.upcast(event));
	}

	getEventsFor(aggregateId: string): Array<Event> {
		return this.events
			.filter((event) => event.aggregateId === aggregateId)
			.map((event) => Events[event.type][`v${event.version}`][event.name].Instance.fromPayload(event.aggregateId, event.creationDate, event.payload))
			.map((event) => this.upcasters.upcast(event));
	}
}

module.exports = InMemoryEventStore;
