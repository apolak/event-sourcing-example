//@flow

import type { Upcaster } from '../upcaster';
import Events from '../../domain/events/credit-application';
import Event from '../../domain/event';
import UserId from '../../domain/user-id';

class OpenProcedureV1ToV2Upcaster implements Upcaster {
  canUpcast(from: Event): boolean {
    return from.name === Events.v1.OpenCreditApplicationProcedureEvent.eventName
      && from.version === 1
      && from.type === Events.v1.OpenCreditApplicationProcedureEvent.type
  }

  upcast(from: Event): Event {
    const userId: UserId = UserId.unknownUser();

    return Events.v2.OpenCreditApplicationProcedureEvent.Instance.fromPayload(
      from.id,
      from.creationDate,
      {
        userId: userId.id
      }
    );
  }
}

module.exports = OpenProcedureV1ToV2Upcaster;
