//@flow

import type { Upcaster } from './upcaster';
import Event from '../domain/event';

type EventUpcasters = {
	[eventName: string]: Array<Upcaster>
};

class Upcasters {
	eventUpcasters: EventUpcasters;

	constructor() {
		this.eventUpcasters = {};
	}

	addUpcaster(eventName: string, upcaster: Upcaster) {
		if (this.eventUpcasters[eventName] === undefined) {
			this.eventUpcasters[eventName] = [upcaster];
		} else {
			this.eventUpcasters[eventName].push(upcaster);
		}
	}

	upcast(event: Event): Event {
		if (this.eventUpcasters[event.name]) {
			return this.eventUpcasters[event.name].reduce((currentEvent, upcaster) => {
				if (upcaster.canUpcast(currentEvent)) {
					return upcaster.upcast(currentEvent);
				}

				return currentEvent;
			}, event);
		}

		return event;
	}
}

module.exports = Upcasters;
