//@flow

import Event from '../../domain/event';

interface Subscriber {
	handle(event: Event): void
}

export type { Subscriber };