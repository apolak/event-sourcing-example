interface CreditHistoryChecker {
  check(creditApplicationId: string, pesel: string): boolean
}

export type { CreditHistoryChecker };
