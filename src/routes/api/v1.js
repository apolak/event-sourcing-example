import express from 'express';

module.exports = (container) => {
	const router = express.Router();

	router.use('/applications', require('./v1/applications.js')(container));
  router.use('/analysis', require('./v1/analysis.js')(container));

	return router;
};
