'use strict';

import CreditApplication from '../../../../domain/credit-application';
import CreditApplicationId from '../../../../domain/credit-application/credit-application-id';
import EventsHistory from '../../../../domain/events-history';

module.exports = (container) => (req, res) => {
  const creditApplicationId = CreditApplicationId.fromString(req.params.id);

  const events = container.services.eventStore.getEventsFor(req.params.id);

  res.json({
    events
  });
};
