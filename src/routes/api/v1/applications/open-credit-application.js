'use strict';

import CreditApplication from '../../../../domain/credit-application';
import CreditApplicationId from '../../../../domain/credit-application/credit-application-id';

module.exports = (container) => (req, res) => {
  const creditApplicationId = CreditApplicationId.generate();
  const creditApplication = new CreditApplication(creditApplicationId);

  creditApplication.openProcedure(req.userId);

  const recentEvents = creditApplication.getRecentEvents();

  container.services.eventStore.push(recentEvents);

  res.json({
    creditApplicationId: creditApplicationId.id
  });
};
