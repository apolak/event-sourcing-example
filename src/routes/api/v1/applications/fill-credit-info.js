'use strict';

import CreditApplication from '../../../../domain/credit-application';
import CreditApplicationId from '../../../../domain/credit-application/credit-application-id';
import EventsHistory from '../../../../domain/events-history';
import CreditInfo from '../../../../domain/credit-application/credit-info';

module.exports = (container) => (req, res) => {
  const creditApplicationId = CreditApplicationId.fromString(req.params.id);

  const creditApplication = CreditApplication.reconstituteFrom(
    new EventsHistory(
      creditApplicationId,
      container.services.eventStore.getEventsFor(req.params.id)
    )
  );

  creditApplication.fillCreditInfo(req.body.creditInfo);

  const recentEvents = creditApplication.getRecentEvents();

  container.services.eventStore.push(recentEvents);

  res.json({
    creditApplicationId: creditApplicationId.id
  });
};
