'use strict';

module.exports = (container) => (req, res) => {
  const events = container.services.eventStore.getEventsFor(req.params.id);

  res.json({
    events
  });
};
