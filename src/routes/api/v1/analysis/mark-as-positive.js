'use strict';

import Analysis from '../../../../domain/analysis';
import AnalysisId from '../../../../domain/analysis/analysis-id';
import AnalysisType from '../../../../domain/analysis/analysis-type';
import EventsHistory from '../../../../domain/events-history';

module.exports = (container) => (req, res) => {
  const analysisId = AnalysisId.fromString(req.params.id);

  const analysis = Analysis.reconstituteFrom(
    new EventsHistory(
      analysisId,
      container.services.eventStore.getEventsFor(req.params.id)
    )
  );

  analysis.markAnalysisAsPositive(AnalysisType.fromType(req.params.type));

  const recentEvents = analysis.getRecentEvents();

  container.services.eventStore.push(recentEvents);

  res.json({
    analysisId: analysisId.id
  });
};
