'use strict';

module.exports = (container) => (req, res) => {
  const applicationToAnalysis = container.repositories.applicationsToBeAnalysedRepository.applications;

  res.json(applicationToAnalysis);
};
