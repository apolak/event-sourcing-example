import express from 'express';

module.exports = (container) => {
  const router = express.Router();

  router.post('/:id/mark-as-positive/:type', require ('./analysis/mark-as-positive.js')(container));
  router.post('/:id/mark-as-negative/:type', require ('./analysis/mark-as-negative.js')(container));
  router.get('/applications-to-analyze', require ('./analysis/applications-to-analyze.js')(container));
  router.get('/:id/events', require ('./analysis/get-analysis-events.js')(container));

  return router;
};
