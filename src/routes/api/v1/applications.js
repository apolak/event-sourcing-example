import express from 'express';

module.exports = (container) => {
	const router = express.Router();

  router.post('/', require ('./applications/open-credit-application.js')(container));
  router.post('/:id/fill-personal-data', require ('./applications/fill-personal-data.js')(container));
  router.post('/:id/fill-financial-data', require ('./applications/fill-financial-data.js')(container));
  router.post('/:id/fill-credit-info', require ('./applications/fill-credit-info.js')(container));
  router.post('/:id/check-credit-history', require ('./applications/check-credit-history.js')(container));
  router.post('/:id/start-analysis', require ('./applications/start-analysis.js')(container));
  router.post('/:id/attach-contract', require ('./applications/attach-contract.js')(container));
  router.post('/:id/presented-to-customer', require ('./applications/presented-to-customer.js')(container));
  router.post('/:id/contract/accept', require ('./applications/accept-contract.js')(container));
  router.post('/:id/contract/reject', require ('./applications/reject-contract.js')(container));
  router.post('/:id/close', require ('./applications/close-credit-application.js')(container));
  router.get('/:id/events', require ('./applications/get-application-events.js')(container));

	return router;
};
