import express from 'express';

module.exports = (container) => {
	const router = express.Router();

	router.use('/', require('./api/v1.js')(container));

  router.get('/load-fixtures', (req, res) => {
  	container.services.fixturesLoader.build();

  	res.json({
  		success: true
	  });
  });

  router.get('/rebuild-opened-projection', (req, res) => {
  	container.projectors.openedApplicationsProjector.rebuild(container.services.eventStore.getAll());

    res.json({
      success: true
    });
  });

	return router;
};
