'use strict';

import express from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';
import userIdMiddleware from './middleware/user-id.middleware';

const createApp = (config) => {
	const app = express();

	app.disable('etag');

	app.use(logger('dev'));
	app.use(cors({
		exposedHeaders: ['user-id', 'content-type', 'accept'],
		allowedHeaders: ['user-id', 'content-type', 'accept']
	}));
	app.use(bodyParser.urlencoded({extended: true}));
	app.use(bodyParser.json());
	app.use(userIdMiddleware);

	app.use('/api', require('./routes/api.js')(config.container));

	app.use((req, res, next) => {
		const err = new Error('Not Found');
		err.status = 404;
		next(err);
	});

	if (app.get('env') === 'development') {
		app.use((err, req, res, next) => {
			res
				.status(err.status || 500)
				.json({
					message: err.message,
					error: err
				});
		});
	}

	app.use((err, req, res, next) => {
		res
			.status(err.status || 500)
			.json({
				message: err.message,
				error: {}
			});
	});

	return app;
};

module.exports = createApp;
