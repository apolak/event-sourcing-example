import type { CreditHistoryChecker } from '../../infrastructure/credit-history-checker';

class HttpCreditHistoryChecker implements CreditHistoryChecker {
  check(creditApplicationId: string, pesel: string) {
    return true;
  }
}

module.exports = HttpCreditHistoryChecker;
