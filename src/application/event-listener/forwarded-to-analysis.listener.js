import CreditApplicationId from '../../domain/credit-application/credit-application-id';
import EventStore from '../../infrastructure/event-store/in-memory';
import Events from '../../domain/events/credit-application';

import Analysis from '../../domain/analysis';
import AnalysisId from '../../domain/analysis/analysis-id';

import type { Subscriber } from '../../infrastructure/event-bus/subscriber';

class ForwardedToAnalysisListener implements Subscriber {
  eventStore: EventStore;

  constructor(eventStore: EventStore) {
    this.eventStore = eventStore;
  }

  handle(event: Event): void {
    if (event.name === Events.v1.ForwardedToAnalysisEvent.eventName && event.version === Events.v1.ForwardedToAnalysisEvent.version) {
      const creditApplicationId: CreditApplicationId = CreditApplicationId.fromString(event.id);

      const analysisId: AnalysisId = AnalysisId.generate();
      const analysis: Analysis = new Analysis(analysisId);

      analysis.connectWithAnalysisTarget(creditApplicationId.id);

      this.eventStore.push(analysis.getRecentEvents());
    }
  }
}

module.exports = ForwardedToAnalysisListener;
