import CreditApplication from '../../domain/credit-application';
import CreditApplicationId from '../../domain/credit-application/credit-application-id';
import EventStore from '../../infrastructure/event-store/in-memory';

import Events from '../../domain/events/credit-application';
import EventsHistory from '../../domain/events-history';
import type { Subscriber } from '../../infrastructure/event-bus/subscriber';
import type { CreditHistoryChecker } from '../../infrastructure/credit-history-checker';

class CreditHistoryCheckListener implements Subscriber {
  eventStore: EventStore;
  creditHistoryChecker: CreditHistoryChecker;

  constructor(eventStore: EventStore, creditHistoryChecker: CreditHistoryChecker) {
    this.eventStore = eventStore;
    this.creditHistoryChecker = creditHistoryChecker;
  }

  handle(event: Event): void {
    if (event.name === Events.v1.CreditHistoryCheckEvent.eventName && event.version === Events.v1.CreditHistoryCheckEvent.version) {
      const creditApplicationId: CreditApplicationId = CreditApplicationId.fromString(event.id);
      const eventsHistory: EventsHistory<CreditApplicationId> = new EventsHistory(
        creditApplicationId,
        this.eventStore.getEventsFor(creditApplicationId.id)
      );
      const creditApplication: CreditApplication = CreditApplication.reconstituteFrom(eventsHistory);

      if (this.creditHistoryChecker.check(event.id, event.payload.pesel)) {
        creditApplication.acceptToAnalysis();
      } else {
        creditApplication.rejectBecauseOfHistoryCheck('Bad history check');
      }

      this.eventStore.push(creditApplication.getRecentEvents());
    }
  }
}

module.exports = CreditHistoryCheckListener;
