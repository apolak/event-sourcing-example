import CreditApplication from '../../domain/credit-application';
import CreditApplicationId from '../../domain/credit-application/credit-application-id';
import EventStore from '../../infrastructure/event-store/in-memory';

import Events from '../../domain/events/analysis';
import EventsHistory from '../../domain/events-history';
import type { Subscriber } from '../../infrastructure/event-bus/subscriber';

class AnalysisFinishedListener implements Subscriber {
  eventStore: EventStore;

  constructor(eventStore: EventStore) {
    this.eventStore = eventStore;
  }

  handle(event: Event): void {
    if (event.name === Events.v1.AnalysisFinishedEvent.eventName && event.version === Events.v1.AnalysisFinishedEvent.version) {
      const creditApplicationId: CreditApplicationId = CreditApplicationId.fromString(event.payload.analysisTargetId);
      const eventsHistory: EventsHistory<CreditApplicationId> = new EventsHistory(
        creditApplicationId,
        this.eventStore.getEventsFor(creditApplicationId.id)
      );
      const creditApplication: CreditApplication = CreditApplication.reconstituteFrom(eventsHistory);

      if (event.payload.result) {
        creditApplication.readyForContract();
      } else {
        creditApplication.rejectBecauseOfAnalysis('Failed because of analysis');
      }

      this.eventStore.push(creditApplication.getRecentEvents());
    }
  }
}

module.exports = AnalysisFinishedListener;
