import CreditApplicationId from '../../domain/credit-application/credit-application-id';
import CreditApplication from '../../domain/credit-application';

import EventStore from '../../infrastructure/event-store/in-memory';
import Events from '../../domain/events/analysis';
import EventsHistory from '../../domain/events-history';

import type { Subscriber } from '../../infrastructure/event-bus/subscriber';

class AnalysisConnectedListener implements Subscriber {
  eventStore: EventStore;

  constructor(eventStore: EventStore) {
    this.eventStore = eventStore;
  }

  handle(event: Event): void {
    if (event.name === Events.v1.AnalysisConnectedWithTargetEvent.eventName && event.version === Events.v1.AnalysisConnectedWithTargetEvent.version) {
      const creditApplicationId: CreditApplicationId = CreditApplicationId.fromString(event.payload.analysisTargetId);
      const eventsHistory: EventsHistory<CreditApplicationId> = new EventsHistory(
        creditApplicationId,
        this.eventStore.getEventsFor(creditApplicationId.id)
      );
      const creditApplication: CreditApplication = CreditApplication.reconstituteFrom(eventsHistory);

      creditApplication.connectWithAnalysis(event.id);

      this.eventStore.push(creditApplication.getRecentEvents());
    }
  }
}

module.exports = AnalysisConnectedListener;
