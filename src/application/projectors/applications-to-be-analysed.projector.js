//@flow

import Events from '../../domain/events/credit-application';
import Event from '../../domain/event';
import InMemoryEventStore from '../../infrastructure/event-store/in-memory';

class ApplicationsToBeAnalysedProjector {
  eventStore: InMemoryEventStore;
  applications: Array<Object>;

  constructor(eventStore: InMemoryEventStore) {
    this.eventStore = eventStore;
    this.applications = [];
  }

  handle(event: Event): void {
    if(event.name === Events.v1.CreditHistoryCheckAcceptedEvent.eventName) {
      const customerPersonalData: Event|null = this.eventStore.findEventOfTypeFor(event.id, Events.v1.PersonalDataFilledEvent.eventName);
      const creditInfo: Event|null = this.eventStore.findEventOfTypeFor(event.id, Events.v1.CreditInfoFilledEvent.eventName);

      if (customerPersonalData && creditInfo) {
        this.applications.push({
          creditApplicationId: event.id,
          creditValue: creditInfo.payload.creditValue,
          creditType: creditInfo.payload.creditType,
          fistName: customerPersonalData.payload.firstName,
          lastName: customerPersonalData.payload.lastName,
          pesel: customerPersonalData.payload.pesel
        });
      }
    }

    if(event.name === Events.v1.ReadyForContractEvent.eventName || event.name === Events.v1.CloseCreditApplicationProcedureEvent.eventName) {
      this.applications = this.applications.filter((application) => application.creditApplicationId !== event.id);
    }
  }
}

module.exports = ApplicationsToBeAnalysedProjector;
