//@flow
import Events from '../../domain/events/credit-application';
import Event from '../../domain/event';
import InMemoryEventStore from '../../infrastructure/event-store/in-memory';

import json2csv from 'json2csv';
import csvjson from 'csvjson';
import fs from 'fs';

class OpenedApplicationsProjector {
  eventStore: InMemoryEventStore;
  xlsPath: string;

  constructor(eventStore: InMemoryEventStore, xlsPath: string) {
    this.eventStore = eventStore;
    this.xlsPath = xlsPath;
  }

  rebuild(events: Array<Events>) {
    if (fs.existsSync(this.xlsPath)) {
      fs.unlinkSync(this.xlsPath);
    }

    events.forEach((event) => this.handle(event));
  }

  handle(event: Event): void {
    if(event.name === Events.v1.CreditInfoFilledEvent.eventName) {
      const customerPersonalData: Event|null = this.eventStore.findEventOfTypeFor(event.id, Events.v1.PersonalDataFilledEvent.eventName);
      const financialData: Event|null = this.eventStore.findEventOfTypeFor(event.id, Events.v1.FinancialDataFilledEvent.eventName);
      const applicationData: Event|null = this.eventStore.findEventOfTypeFor(event.id, Events.v2.OpenCreditApplicationProcedureEvent.eventName);

      if (applicationData && customerPersonalData && financialData) {
        if (fs.existsSync(this.xlsPath)) {
          const applications = csvjson.toObject(fs.readFileSync(this.xlsPath, 'utf-8'));

          applications.push({
            id: event.id,
            agentId: applicationData.payload.userId,
            firstName: customerPersonalData.payload.firstName,
            lastName: customerPersonalData.payload.lastName,
            city: customerPersonalData.payload.city,
            phone: customerPersonalData.payload.phone,
            earnings: financialData.payload.earnings,
            creditValue: event.payload.creditValue,
            creditType: event.payload.creditType,
          });

          const csv = json2csv({data: applications, quotes: ''});

          fs.writeFileSync(this.xlsPath, csv);
        } else {
          const applications = [{
            id: event.id,
            agentId: applicationData.payload.userId,
            firstName: customerPersonalData.payload.firstName,
            lastName: customerPersonalData.payload.lastName,
            city: customerPersonalData.payload.city,
            phone: customerPersonalData.payload.phone,
            earnings: financialData.payload.earnings,
            creditValue: event.payload.creditValue,
            creditType: event.payload.creditType,
          }];

          const csv = json2csv({data: applications, quotes: ''});

          fs.writeFileSync(this.xlsPath, csv);
        }
      }
    }

    if(event.name === Events.v1.CloseCreditApplicationProcedureEvent.eventName) {
      if (fs.existsSync(this.xlsPath)) {
        const applications = csvjson.toObject(fs.readFileSync(this.xlsPath, 'utf-8'));
        const filteredApplications = applications.filter((application) => application.id !== event.id);
        if (filteredApplications.length > 0) {
          const csv = json2csv({data: filteredApplications, quotes: ''});
          fs.writeFileSync(this.xlsPath, csv);
        } else {
          fs.writeFileSync(this.xlsPath, '');
        }
      }
    }
  }
}

module.exports = OpenedApplicationsProjector;
