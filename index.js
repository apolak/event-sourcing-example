import createApp from './src/app';
import config from './config/prod.config';

const appConfig = config();
const app = createApp(appConfig);

app.listen(appConfig.port);
console.log(`server started on port ${appConfig.port}`);
