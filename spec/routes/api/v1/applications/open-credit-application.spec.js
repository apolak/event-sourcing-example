import request from 'supertest';
import app from '../../../../../src/app';
import config from '../../../../../config/test.config';
import UserId from '../../../../../src/domain/user-id';
import CreditApplicationId from '../../../../../src/domain/credit-application/credit-application-id';
import fireBeforeEach from '../../../../../src/tests/before';

import { expect } from 'chai';

describe('API', () => {
	'use strict';

  fireBeforeEach(config().paths.applicationsCsvFilename);

	it('POST /api/applications', (done) => {
		const appConfig = config();

		request(app(appConfig).listen()).post('/api/applications')
			.expect(200)
			.end((err, res) => {
				expect(CreditApplicationId.isValidId(res.body.creditApplicationId)).to.be.true;
				expect(UserId.isValidId(res.headers['user-id'])).to.be.true;

				const orderEventsHistory = appConfig.container.services.eventStore.getEventsFor(res.body.creditApplicationId);

				expect(orderEventsHistory.length).to.equal(1);

				done();
			});
	});

  it('POST /api/applications with user id', (done) => {
    const userId = UserId.generate();
    const appConfig = config();

    request(app(appConfig).listen()).post('/api/applications')
      .expect(200)
      .set('user-id', userId.id)
      .end((err, res) => {
        expect(CreditApplicationId.isValidId(res.body.creditApplicationId)).to.be.true;
        expect(res.headers['user-id']).to.equal(userId.id);

        const applicationEventsHistory = appConfig.container.services.eventStore.getEventsFor(res.body.creditApplicationId);
        expect(applicationEventsHistory.length).to.equal(1);

        done();
      });
  });
});
