import request from 'supertest';
import app from '../../../../../src/app';
import config from '../../../../../config/test.config';
import UserId from '../../../../../src/domain/user-id';
import CreditApplicationId from '../../../../../src/domain/credit-application/credit-application-id';
import CreditApplicationEventsBuilder from '../../../../../src/tests/events-builder/credit-application-events-builder';
import fireBeforeEach from '../../../../../src/tests/before';

import { expect } from 'chai';

describe('API', () => {
  'use strict';

  fireBeforeEach(config().paths.applicationsCsvFilename);

  it('POST /api/applications/:id/fill-personal-data with user id', (done) => {
    const userId = UserId.generate();
    const creditApplicationId = CreditApplicationId.generate();
    const appConfig = config();

    appConfig.container.services.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withOpenProcedureEvent()
        .getEvents()
    );

    request(app(appConfig).listen()).post('/api/applications/' + creditApplicationId.id + '/fill-personal-data')
      .expect(200)
      .send({
        personalData: {
          firstName: 'John',
          lastName: 'Doe',
          pesel: '123123123',
          documentId: 'AHD123123',
          phone: '123456123',
          city: 'Warsaw'
        }
      })
      .set('user-id', userId.id)
      .end((err, res) => {
        expect(res.body.creditApplicationId).to.equal(creditApplicationId.id);
        expect(res.headers['user-id']).to.equal(userId.id);

        const applicationEventsHistory = appConfig.container.services.eventStore.getEventsFor(res.body.creditApplicationId);
        expect(applicationEventsHistory.length).to.equal(2);

        done();
      });
  });
});
