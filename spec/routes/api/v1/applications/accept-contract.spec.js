import request from 'supertest';
import app from '../../../../../src/app';
import config from '../../../../../config/test.config';
import UserId from '../../../../../src/domain/user-id';
import CreditApplicationId from '../../../../../src/domain/credit-application/credit-application-id';
import CreditApplicationEventsBuilder from '../../../../../src/tests/events-builder/credit-application-events-builder';
import fireBeforeEach from '../../../../../src/tests/before';

import { expect } from 'chai';

describe('API', () => {
  'use strict';

  fireBeforeEach(config().paths.applicationsCsvFilename);

  it('POST /api/applications/:id/contract/accept with user id', (done) => {
    const userId = UserId.generate();
    const creditApplicationId = CreditApplicationId.generate();
    const appConfig = config();

    appConfig.container.services.creditHistoryChecker.check = () => true;

    appConfig.container.services.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withOpenProcedureEvent()
        .withPersonalDataFilledEvent()
        .withFinancialDataFilledEvent()
        .withCreditInfoFilledEvent()
        .withCreditHistoryCheckEvent()
        .getEvents()
    );

    appConfig.container.services.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withAnalysisStartedEvent()
        .withReadyForContractEvent()
        .withReadyForPresentationEvent()
        .withContractPresentedEvent()
        .getEvents()
    );

    request(app(appConfig).listen()).post('/api/applications/' + creditApplicationId.id + '/contract/accept')
      .expect(200)
      .set('user-id', userId.id)
      .end((err, res) => {
        expect(res.body.creditApplicationId).to.equal(creditApplicationId.id);
        expect(res.headers['user-id']).to.equal(userId.id);

        const applicationEventsHistory = appConfig.container.services.eventStore.getEventsFor(res.body.creditApplicationId);
        expect(applicationEventsHistory.length).to.equal(12);

        done();
      });
  });
});
