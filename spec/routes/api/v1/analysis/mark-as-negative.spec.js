import request from 'supertest';
import app from '../../../../../src/app';
import config from '../../../../../config/test.config';
import UserId from '../../../../../src/domain/user-id';
import AnalysisId from '../../../../../src/domain/analysis/analysis-id';
import CreditApplicationId from '../../../../../src/domain/credit-application/credit-application-id';

import Events from '../../../../../src/domain/events/analysis';

import { expect } from 'chai';

describe('API', () => {
  'use strict';

  it('POST /api/analysis/:id/mark-as-negative personal background analysis with user id', (done) => {
    const userId = UserId.generate();
    const analysisId = AnalysisId.generate();
    const creditApplicationId = CreditApplicationId.generate();
    const appConfig = config();

    appConfig.container.services.eventStore.push([
      Events.v1.AnalysisConnectedWithTargetEvent.Instance.withoutCreationDate(analysisId.id, creditApplicationId.id)
    ]);

    request(app(appConfig).listen()).post('/api/analysis/' + analysisId.id + '/mark-as-negative/personal-background')
      .expect(200)
      .set('user-id', userId.id)
      .end((err, res) => {
        expect(res.body.analysisId).to.equal(analysisId.id);
        expect(res.headers['user-id']).to.equal(userId.id);

        const analysisEventsHistory = appConfig.container.services.eventStore.getEventsFor(res.body.analysisId);
        expect(analysisEventsHistory.length).to.equal(3);

        const applicationEventsHistory = appConfig.container.services.eventStore.getEventsFor(creditApplicationId.id);
        expect(applicationEventsHistory.length).to.equal(3);

        done();
      });
  });

  it('POST /api/analysis/:id/mark-as-negative previous credits analysis with user id', (done) => {
    const userId = UserId.generate();
    const analysisId = AnalysisId.generate();
    const creditApplicationId = CreditApplicationId.generate();
    const appConfig = config();

    appConfig.container.services.eventStore.push([
      Events.v1.AnalysisConnectedWithTargetEvent.Instance.withoutCreationDate(analysisId.id, creditApplicationId.id)
    ]);

    request(app(appConfig).listen()).post('/api/analysis/' + analysisId.id + '/mark-as-negative/previous-credits')
      .expect(200)
      .set('user-id', userId.id)
      .end((err, res) => {
        expect(res.body.analysisId).to.equal(analysisId.id);
        expect(res.headers['user-id']).to.equal(userId.id);

        const analysisEventsHistory = appConfig.container.services.eventStore.getEventsFor(res.body.analysisId);
        expect(analysisEventsHistory.length).to.equal(3);

        const applicationEventsHistory = appConfig.container.services.eventStore.getEventsFor(creditApplicationId.id);
        expect(applicationEventsHistory.length).to.equal(3);

        done();
      });
  });

  it('POST /api/analysis/:id/mark-as-negative earnings analysis with user id', (done) => {
    const userId = UserId.generate();
    const analysisId = AnalysisId.generate();
    const creditApplicationId = CreditApplicationId.generate();
    const appConfig = config();

    appConfig.container.services.eventStore.push([
      Events.v1.AnalysisConnectedWithTargetEvent.Instance.withoutCreationDate(analysisId.id, creditApplicationId.id)
    ]);

    request(app(appConfig).listen()).post('/api/analysis/' + analysisId.id + '/mark-as-negative/earnings')
      .expect(200)
      .set('user-id', userId.id)
      .end((err, res) => {
        expect(res.body.analysisId).to.equal(analysisId.id);
        expect(res.headers['user-id']).to.equal(userId.id);

        const analysisEventsHistory = appConfig.container.services.eventStore.getEventsFor(res.body.analysisId);
        expect(analysisEventsHistory.length).to.equal(3);

        const applicationEventsHistory = appConfig.container.services.eventStore.getEventsFor(creditApplicationId.id);
        expect(applicationEventsHistory.length).to.equal(3);

        done();
      });
  });
});
