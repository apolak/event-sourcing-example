import request from 'supertest';
import app from '../../../../../src/app';
import config from '../../../../../config/test.config';
import UserId from '../../../../../src/domain/user-id';
import CreditApplicationId from '../../../../../src/domain/credit-application/credit-application-id';
import CreditApplicationEventsBuilder from '../../../../../src/tests/events-builder/credit-application-events-builder';

import { expect } from 'chai';

describe('API', () => {
  'use strict';

  it('POST /api/analysis/applications-to-analyze', (done) => {
    const userId = UserId.generate();
    const creditApplicationId = CreditApplicationId.generate();
    const appConfig = config();

    appConfig.container.services.creditHistoryChecker.check = () => true;

    appConfig.container.services.eventStore.push(
      new CreditApplicationEventsBuilder(creditApplicationId)
        .withOpenProcedureEvent()
        .withPersonalDataFilledEvent()
        .withFinancialDataFilledEvent()
        .withCreditInfoFilledEvent()
        .withCreditHistoryCheckEvent()
        .getEvents()
    );

    request(app(appConfig).listen()).get('/api/analysis/applications-to-analyze')
      .expect(200)
      .set('user-id', userId.id)
      .end((err, res) => {
        if (err) return done(err);

        expect(res.body.length).to.equal(1);
        expect(res.body[0].creditValue).to.match(/\d+/);
        expect(res.body[0].creditType).to.match(/\w+/);
        expect(res.body[0].lastName).to.match(/\w+/);
        expect(res.body[0].pesel).to.match(/\w+/);
        expect(CreditApplicationId.isValidId(res.body[0].creditApplicationId)).to.equal(true);

        done();
      });
  });
});
