import InMemoryEventStore from '../src/infrastructure/event-store/in-memory';
import EventBus from '../src/infrastructure/event-bus';
import Upcasters from '../src/infrastructure/upcasters';
import CreditHistoryChecker from '../src/application/service/credit-history-checker';
import CreditHistoryCheckListener from '../src/application/event-listener/credit-history-check.listener';
import ForwardedToAnalysisListener from '../src/application/event-listener/forwarded-to-analysis.listener';
import AnalysisConnectedListener from '../src/application/event-listener/analysis-connected.listener';
import AnalysisFinishedListener from '../src/application/event-listener/analysis-finished.listener';

import ApplicationsToBeAnalysedProjector from '../src/application/projectors/applications-to-be-analysed.projector';
import OpenedApplicationProjector from '../src/application/projectors/opened-applications.projector';

import CreditApplicationEvents from '../src/domain/events/credit-application';

import OpenProcedureV1ToV2Upcaster from '../src/infrastructure/upcaster/open-procedure-v1-to-v2.upcaster';

module.exports = () => {
	'use strict';
	const applicationsCsvFilename = __dirname + '/../public/openApplications-test.csv';

  const upcasters = new Upcasters();
	const eventBus = new EventBus();
	const eventStore = new InMemoryEventStore(eventBus, upcasters);

	const creditHistoryChecker = new CreditHistoryChecker();
	const creditHistoryCheckListener = new CreditHistoryCheckListener(eventStore, creditHistoryChecker);

	const forwardedToAnalysisListener = new ForwardedToAnalysisListener(eventStore);
	const analysisConnectedListener = new AnalysisConnectedListener(eventStore);
  const analysisFinishedListener = new AnalysisFinishedListener(eventStore);
  const applicationsToBeAnalysedProjector = new ApplicationsToBeAnalysedProjector(eventStore);
  const openedApplicationsProjector = new OpenedApplicationProjector(eventStore, applicationsCsvFilename);

  upcasters.addUpcaster(CreditApplicationEvents.v1.OpenCreditApplicationProcedureEvent.eventName, new OpenProcedureV1ToV2Upcaster());

	eventBus.subscribe(creditHistoryCheckListener);
	eventBus.subscribe(forwardedToAnalysisListener);
	eventBus.subscribe(analysisConnectedListener);
	eventBus.subscribe(analysisFinishedListener);
	eventBus.subscribe(applicationsToBeAnalysedProjector);
	eventBus.subscribe(openedApplicationsProjector);

	return {
		port: 5000,
		paths: {
      applicationsCsvFilename: applicationsCsvFilename
		},
		container: {
			services: {
				eventStore: eventStore,
				creditHistoryChecker: creditHistoryChecker
			},
			repositories: {
				applicationsToBeAnalysedRepository: applicationsToBeAnalysedProjector
			}
		}
	};
};
