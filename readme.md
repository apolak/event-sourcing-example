#How to run
``` 
yarn 

yarn start

yarn test
```
#Flow

###POST /api/applications
It return credit application id used for other endpoints
###POST /api/applications/:id/fill-personal-data
```
{
	"personalData": {
		"firstName": "John",
		"lastName": "Doe",
		"phone": "123123123",
		"pesel": "123123123",
		"documentId": "123123123",
		"city": "Gliwice"
	}
}
```
###POST /api/applications/:id/fill-financial-data
```
{
	"financialData": {
		"earnings": 10000
	}
}
```

###POST /api/applications/:id/fill-credit-info
```
{
	"creditInfo": {
		"creditValue": 2000,
		"creditType": "hipoteczny"
	}
}
```

###POST /api/applications/:id/check-credit-history

###POST /api/applications/:id/start-analysis

It return analysis id used as id for analysis endpoints

###POST /api/analysis/:analysisId/mark-as-positive/personal-background
###POST /api/analysis/:analysisId/mark-as-positive/previous-credit
###POST /api/analysis/:analysisId/mark-as-positive/earnings

###(Alternatively you can mark analysis as negative)
###POST /api/analysis/:analysisId/mark-as-negative/earnings

###POST /api/applications/:id/attach-contract
###POST /api/applications/:id/presented-to-customer
###POST /api/applications/:id/contract/accept

###(Alternatively you can reject contract)
###POST /api/applications/:id/contract/reject

###POST /api/applications/:id/close

####Close can be executed at any given time


#Projections

### CSV file in public directory - filled after `fill-credit-info`, items removed after `close procedure event`
###GET /api/analysis//applications-to-analyze - applications that are to be analysed
 
 
#Debug
 
### GET /api/applications/:id/event - all events of given aggregate (does not matter if application or analysis)
### GET /api/load-fixtures - fill in memory store with fixtures
### GET /api/rebuild-opened-projection - rebuild csv projection
